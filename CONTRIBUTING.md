# Setup

To setup clone this repo and switch the workspace into the project dir and run `docker-compose up -d`

In the next step you need to install the project's dependencies. To do this run `./composer.sh install`

# UnitTests

You can run the unit-tests via `./phpunit.sh`.
Please notice, that we aim for 100% Coverage.

# PreCommit

You can run manually `./precommit.sh` to validate codestyle, run lint check (php) and run phpunit or you can link it as hook `cd .git/hooks/ && ln -s ../../precommit.sh pre-commit && cd -` to run it automatically before every git commit.
