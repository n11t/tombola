<?php
declare(strict_types=1);

namespace N11t\Tombola\Output\Image;

interface GetImageOutput
{

    /**
     * @param string $fileName
     */
    public function setFilename(string $fileName);
}
