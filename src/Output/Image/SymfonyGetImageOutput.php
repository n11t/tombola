<?php
declare(strict_types=1);

namespace N11t\Tombola\Output\Image;

class SymfonyGetImageOutput implements GetImageOutput
{

    /**
     * @var string
     */
    private $fileName;

    /**
     * @param string $fileName
     */
    public function setFilename(string $fileName)
    {
        $this->fileName = $fileName;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->fileName;
    }
}
