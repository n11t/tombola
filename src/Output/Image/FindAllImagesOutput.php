<?php
declare(strict_types=1);

namespace N11t\Tombola\Output\Image;

interface FindAllImagesOutput
{

    /**
     * Set the basenames as string array.
     *
     * @param array $images
     */
    public function setImages(array $images);
}
