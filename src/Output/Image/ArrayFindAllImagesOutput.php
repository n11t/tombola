<?php
declare(strict_types=1);

namespace N11t\Tombola\Output\Image;

class ArrayFindAllImagesOutput implements FindAllImagesOutput
{

    /**
     * @var string[]
     */
    private $images;

    public function __construct()
    {
        $this->images = [];
    }

    /**
     * Set the basenames as string array.
     *
     * @param string[] $images
     */
    public function setImages(array $images)
    {
        $this->images = $images;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->images;
    }
}
