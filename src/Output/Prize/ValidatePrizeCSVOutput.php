<?php

namespace N11t\Tombola\Output\Prize;

interface ValidatePrizeCSVOutput
{

    /**
     * Add a file error.
     *
     * @param string $error
     */
    public function addFileError(string $error);

    /**
     * @param array $errors
     */
    public function setRowErrors(array $errors);
}
