<?php
declare(strict_types=1);

namespace N11t\Tombola\Output\Prize;

class ArrayUpdateBlankOutput implements UpdateBlankOutput
{

    /**
     * @var
     */
    private $errors;

    public function __construct()
    {
        $this->errors = [];
    }

    /**
     * Add an error thrown by validating the blank.
     *
     * @param array $blank
     * @param string $error
     */
    public function addBlankError(array $blank, string $error)
    {
        $this->errors[] = $error;
    }

    /**
     * Return true if no error exists.
     *
     * @return bool
     */
    public function isSuccess(): bool
    {
        return count($this->errors) < 1;
    }

    /**
     * Return errors as array
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->errors;
    }
}
