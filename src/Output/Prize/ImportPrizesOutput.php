<?php
declare(strict_types=1);

namespace N11t\Tombola\Output\Prize;

interface ImportPrizesOutput
{

    /**
     * Add an error for given prize.
     *
     * @param array $prize
     * @param string $error
     */
    public function addError(array $prize, string $error);
}
