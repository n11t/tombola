<?php
declare(strict_types=1);

namespace N11t\Tombola\Output\Prize;

interface UpdateBlankOutput
{

    /**
     * Add an error thrown by validating the blank.
     *
     * @param array $blank
     * @param string $error
     */
    public function addBlankError(array $blank, string $error);
}
