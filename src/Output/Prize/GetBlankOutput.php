<?php
declare(strict_types=1);

namespace N11t\Tombola\Output\Prize;

interface GetBlankOutput
{

    /**
     * Set the blank
     *
     * @param array $blank
     */
    public function setBlank(array $blank);
}
