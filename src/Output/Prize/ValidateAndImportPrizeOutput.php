<?php

namespace N11t\Tombola\Output\Prize;

class ValidateAndImportPrizeOutput implements ValidatePrizeCSVOutput, ImportPrizesOutput
{

    /**
     * @var array
     */
    private $errors;

    public function __construct()
    {
        $this->errors = [
            'file' => [],
            'rows' => [],
            'prize' => []
        ];
    }

    /**
     * Add a file error.
     *
     * @param string $error
     */
    public function addFileError(string $error)
    {
        $this->errors['file'][] = $error;
    }

    /**
     * @param array $errors
     */
    public function setRowErrors(array $errors)
    {
        $this->errors['rows'] = $errors;
    }

    /**
     * {@inheritdoc}
     */
    public function addError(array $prize, string $error)
    {
        $this->errors['prize'][$prize['id']] = $error;
    }

    /***
     * @return array
     */
    public function toArray(): array
    {
        return $this->errors;
    }

    /**
     * @return array
     */
    public function getFileErrors(): array
    {
        return $this->errors['file'];
    }

    /**
     * @return array
     */
    public function getRowErrors(): array
    {
        return $this->errors['rows'];
    }

    /**
     * @return array
     */
    public function getPrizeErrors(): array
    {
        return $this->errors['prize'];
    }
}
