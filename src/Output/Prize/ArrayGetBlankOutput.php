<?php
declare(strict_types=1);

namespace N11t\Tombola\Output\Prize;

class ArrayGetBlankOutput implements GetBlankOutput
{

    /**
     * @var array
     */
    private $blank;

    public function __construct()
    {
        $this->blank = [];
    }

    /**
     * Set the blank
     *
     * @param array $blank
     */
    public function setBlank(array $blank)
    {
        $this->blank = $blank;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->blank;
    }
}
