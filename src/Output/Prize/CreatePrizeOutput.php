<?php
declare(strict_types=1);

namespace N11t\Tombola\Output\Prize;

interface CreatePrizeOutput
{

    /**
     * @param array $prize
     * @param string $error
     */
    public function setCreatePrizeError(array $prize, string $error);
}
