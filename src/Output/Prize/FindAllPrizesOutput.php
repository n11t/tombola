<?php

namespace N11t\Tombola\Output\Prize;

interface FindAllPrizesOutput
{

    /**
     * Set the prizes.
     *
     * @param array $prizes
     */
    public function setPrizes(array $prizes);
}
