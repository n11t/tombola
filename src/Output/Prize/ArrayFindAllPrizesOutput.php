<?php

namespace N11t\Tombola\Output\Prize;

class ArrayFindAllPrizesOutput implements FindAllPrizesOutput
{

    /**
     * @var array
     */
    private $prizes;

    /**
     * Set the prizes.
     *
     * @param array $prizes
     */
    public function setPrizes(array $prizes)
    {
        $this->prizes = $prizes;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return $this->prizes;
    }
}
