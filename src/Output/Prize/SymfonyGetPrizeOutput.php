<?php
declare(strict_types=1);

namespace N11t\Tombola\Output\Prize;

class SymfonyGetPrizeOutput implements GetPrizeOutput
{

    /**
     * @var array
     */
    private $prize;

    public function __construct()
    {
        $this->prize = [];
    }

    /**
     * @param array $prize
     */
    public function setPrize(array $prize)
    {
        $this->prize = $prize;
    }

    /**
     * @return array
     */
    public function getPrize(): array
    {
        return $this->prize;
    }
}
