<?php
declare(strict_types=1);

namespace N11t\Tombola\Output\Prize;

class ArrayCreatePrizeOutput implements CreatePrizeOutput
{

    /**
     * @var array
     */
    private $errors;

    public function __construct()
    {
        $this->errors = [];
    }

    /**
     * @param array $prize
     * @param string $error
     */
    public function setCreatePrizeError(array $prize, string $error)
    {
        $this->errors[] = $error;
    }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'errors' => $this->errors
        ];
    }

    /**
     * @return bool
     */
    public function isSuccess(): bool
    {
        return count($this->errors) < 1;
    }
}
