<?php
declare(strict_types=1);

namespace N11t\Tombola\Output\Prize;

interface GetPrizeOutput
{

    /**
     * @param array $prize
     */
    public function setPrize(array $prize);
}
