<?php
declare(strict_types=1);

namespace N11t\Tombola\Input\Prize;

use N11t\Tombola\Input\Prize\GetPrizeInput;
use Symfony\Component\HttpFoundation\Request;

class SymfonyGetPrizeInput implements GetPrizeInput
{

    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int) $this->request->attributes->get('id', 0);
    }
}
