<?php
declare(strict_types=1);

namespace N11t\Tombola\Input\Prize;

interface UpdateBlankInput
{

    /**
     * Get the blank to update.
     *
     * @return array
     */
    public function getBlank(): array;
}
