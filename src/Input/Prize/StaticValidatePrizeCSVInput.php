<?php

namespace N11t\Tombola\Input\Prize;

class StaticValidatePrizeCSVInput implements ValidatePrizeCSVInput
{

    /**
     * @var string
     */
    private $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Return the file path.
     *
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }
}
