<?php

namespace N11t\Tombola\Input\Prize;

use Symfony\Component\HttpFoundation\Request;

class SymfonyUpdatePrizeInput implements UpdatePrizeInput
{

    /**
     * @var Request
     */
    public $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get the id to identify with prize to update
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->request->attributes->getInt('id');
    }

    /**
     * The description to set.
     *
     * @return string|null
     */
    public function getDescription(): string
    {
        return $this->request->request->get('description', '');
    }

    /**
     * The image to set.
     *
     * @return string|null
     */
    public function getImage(): string
    {
        return $this->request->request->get('image', '');
    }
}
