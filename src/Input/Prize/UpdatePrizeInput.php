<?php
declare(strict_types=1);

namespace N11t\Tombola\Input\Prize;

interface UpdatePrizeInput
{

    /**
     * Get the id to identify with prize to update
     *
     * @return int
     */
    public function getId(): int;

    /**
     * The description to set.
     *
     * @return string|null
     */
    public function getDescription();

    /**
     * The image to set.
     *
     * @return string|null
     */
    public function getImage();
}
