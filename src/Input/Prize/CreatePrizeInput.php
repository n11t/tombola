<?php
declare(strict_types=1);

namespace N11t\Tombola\Input\Prize;

interface CreatePrizeInput
{

    /**
     * Get the prize to create.
     *
     * @return array
     */
    public function getPrize(): array;
}
