<?php
declare(strict_types=1);

namespace N11t\Tombola\Input\Prize;

use Symfony\Component\HttpFoundation\Request;

class SymfonyCreatePrizeInput implements CreatePrizeInput
{

    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return int
     */
    private function getId(): int
    {
        return $this->request->request->getInt('id');
    }

    /**
     * @return string
     */
    private function getImage(): string
    {
        return $this->request->request->get('image');
    }

    /**
     * @return string
     */
    private function getDescription(): string
    {
        return $this->request->request->get('description');
    }

    /**
     * Get the prize to create.
     *
     * @return array
     */
    public function getPrize(): array
    {
        return [
            'id' => $this->getId(),
            'image' => $this->getImage(),
            'description' => $this->getDescription()
        ];
    }
}
