<?php
declare(strict_types=1);

namespace N11t\Tombola\Input\Prize;

interface DeletePrizeInput
{

    /**
     * Return the id to delete the prize.
     *
     * @return int
     */
    public function getId(): int;
}
