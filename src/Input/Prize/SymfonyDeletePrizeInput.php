<?php

namespace N11t\Tombola\Input\Prize;

use Symfony\Component\HttpFoundation\Request;

class SymfonyDeletePrizeInput implements DeletePrizeInput
{

    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Return the id to delete the prize.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->request->attributes->get('id', 0);
    }
}
