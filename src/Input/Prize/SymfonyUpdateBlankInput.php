<?php
declare(strict_types=1);

namespace N11t\Tombola\Input\Prize;

use Symfony\Component\HttpFoundation\Request;

class SymfonyUpdateBlankInput implements UpdateBlankInput
{

    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Get the blank to update.
     *
     * @return array
     */
    public function getBlank(): array
    {
        return [
            'image' => $this->request->request->get('image', ''),
            'description' => $this->request->request->get('description', '')
        ];
    }
}
