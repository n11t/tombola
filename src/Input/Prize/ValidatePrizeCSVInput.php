<?php

namespace N11t\Tombola\Input\Prize;

interface ValidatePrizeCSVInput
{

    /**
     * Return the file path.
     *
     * @return string
     */
    public function getFilePath(): string;
}
