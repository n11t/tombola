<?php
declare(strict_types=1);

namespace N11t\Tombola\Input\Prize;

interface GetPrizeInput
{

    /**
     * @return int
     */
    public function getId(): int;
}
