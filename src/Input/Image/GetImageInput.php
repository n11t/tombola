<?php
declare(strict_types=1);

namespace N11t\Tombola\Input\Image;

interface GetImageInput
{

    /**
     * @return int
     */
    public function getId(): int;
}
