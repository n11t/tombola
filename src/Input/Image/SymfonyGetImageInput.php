<?php
declare(strict_types=1);

namespace N11t\Tombola\Input\Image;

use Symfony\Component\HttpFoundation\Request;

class SymfonyGetImageInput implements GetImageInput
{

    /**
     * @var Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return (int) $this->request->attributes->get('id', 0);
    }
}
