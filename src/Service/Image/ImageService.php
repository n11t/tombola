<?php
declare(strict_types=1);

namespace N11t\Tombola\Service\Image;

interface ImageService
{

    /**
     * @param array $prize
     * @return string
     * @throws ImageNotFoundException
     */
    public function findImage(array $prize): string;

    /**
     * Find all images and return the basename.
     *
     * @return string[]
     */
    public function findAll(): array;
}
