<?php
declare(strict_types=1);

namespace N11t\Tombola\Service\Image;

class ImageNotFoundException extends \Exception
{

    public function __construct(string $directory)
    {
        $message = sprintf(
            "File not found or file not readable: '%s'",
            $directory
        );

        parent::__construct($message, 404);
    }
}
