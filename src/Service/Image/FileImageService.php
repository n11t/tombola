<?php
declare(strict_types=1);

namespace N11t\Tombola\Service\Image;

class FileImageService implements ImageService
{

    /**
     * @var string
     */
    private $directory;

    public function __construct(string $directory)
    {
        $this->directory = rtrim($directory, '/');
    }

    /**
     * @param array $prize
     * @return string
     * @throws ImageNotFoundException
     */
    public function findImage(array $prize): string
    {
        $fileName = $this->directory . '/' . $prize['image'];

        if (!\is_file($fileName) || !\is_readable($fileName)) {
            throw new ImageNotFoundException($fileName);
        }

        return $fileName;
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        $iterator = new \DirectoryIterator($this->directory);

        $files = [];
        foreach ($iterator as $fileInfo) {
            if ($fileInfo->isDot()) {
                continue;
            }

            $files[] = $fileInfo->getBasename();
        }

        return $files;
    }
}
