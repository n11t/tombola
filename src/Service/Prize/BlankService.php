<?php
declare(strict_types=1);

namespace N11t\Tombola\Service\Prize;

interface BlankService
{

    /**
     * @return array
     */
    public function getBlank(): array;

    /**
     * Update the blank.
     *
     * @param array $blank
     */
    public function updateBlank(array $blank);
}
