<?php
declare(strict_types=1);

namespace N11t\Tombola\Service\Prize;

use Throwable;

class DuplicatePrizeException extends \Exception
{

    /**
     * @var array
     */
    private $prize;

    public function __construct(array $prize)
    {
        $message = sprintf(
            'Duplicate prize for id %d',
            $prize['id']
        );

        parent::__construct($message, 409);
        $this->prize = $prize;
    }

    /**
     * @return array
     */
    public function getPrize(): array
    {
        return $this->prize;
    }
}
