<?php
declare(strict_types=1);

namespace N11t\Tombola\Service\Prize;

class CSVImportService implements ImportService
{
    const CSV_KEY_MIN = 0;

    const CSV_KEY_MAX = 1;

    const CSV_KEY_IMAGE = 2;

    const CSV_KEY_DESCRIPTION = 3;

    /**
     * @var string
     */
    private $csvFile;

    public function __construct(string $file)
    {
        $this->csvFile = $file;
    }

    /**
     * Get all prizes to import
     *
     * @return array
     */
    public function getPrizesToImport(): array
    {
        $csvContent = $this->parseCSVFile();

        return $this->convertCSVContentToPrizes($csvContent);
    }

    /**
     * Convert the csv rows to prizes
     *
     * @param array $csvContent
     * @return array
     */
    private function convertCSVContentToPrizes(array $csvContent): array
    {
        $prizes = [];
        foreach ($csvContent as $row) {
            $min = $row[self::CSV_KEY_MIN];
            $max = $row[self::CSV_KEY_MAX];
            for ($i = $min; $i <= $max; $i++) {
                $prizes[$i] = [
                    'id' => (int)$i,
                    'image' => $row[self::CSV_KEY_IMAGE],
                    'description' => $row[self::CSV_KEY_DESCRIPTION]
                ];
            }
        }

        return $prizes;
    }

    /**
     * @return array
     */
    protected function parseCSVFile(): array
    {
        $content = [];

        $handle = fopen($this->csvFile, 'rb');
        while (false !== ($row = fgetcsv($handle))) {
            $content[] = $row;
        }
        fclose($handle);

        usort($content, function (array $row1, array $row2) {
            return $row1[self::CSV_KEY_MIN] <=> $row2[self::CSV_KEY_MIN];
        });

        return $content;
    }
}
