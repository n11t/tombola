<?php
declare(strict_types=1);

namespace N11t\Tombola\Service\Prize;

class PrizeNotFoundException extends \Exception
{

    /**
     * @var array
     */
    private $params;

    public function __construct(array $params)
    {
        $message = sprintf(
            'Can\'t find prize for params: %s',
            json_encode($params)
        );

        parent::__construct($message, 400);

        $this->params = $params;
    }

    /**
     * @return array
     */
    public function getParams(): array
    {
        return $this->params;
    }
}
