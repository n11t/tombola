<?php
declare(strict_types=1);

namespace N11t\Tombola\Service\Prize;

interface ImportService
{

    /**
     * Get all prizes to import
     *
     * @return array
     */
    public function getPrizesToImport(): array;
}
