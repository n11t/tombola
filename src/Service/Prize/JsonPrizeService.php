<?php
declare(strict_types=1);

namespace N11t\Tombola\Service\Prize;

class JsonPrizeService implements PrizeService
{

    /**
     * @var string
     */
    private $jsonFile;

    public function __construct(string $jsonFile)
    {
        $this->jsonFile = $jsonFile;

        if (!is_file($this->jsonFile)) {
            $this->writeJsonFile([]);
        }
    }

    /**
     * Find prize by given id.
     *
     * @param int $id
     * @return array
     * @throws PrizeNotFoundException
     */
    public function findById(int $id): array
    {
        $prizes = $this->parseJsonFile();

        if (array_key_exists($id, $prizes)) {
            return $prizes[$id];
        }

        throw new PrizeNotFoundException(['id'=>$id]);
    }

    /**
     * Persists given prize.
     *
     * @param array $prize
     * @throws DuplicatePrizeException
     */
    public function addPrize(array $prize)
    {
        $prizes = $this->parseJsonFile();

        $id = $prize['id'];
        if (array_key_exists($id, $prizes)) {
            throw new DuplicatePrizeException($prize);
        }

        $prizes[$prize['id']] = $prize;

        $this->writeJsonFile($prizes);
    }

    /**
     * {@inheritdoc}
     */
    public function deletePrize(int $id)
    {
        $prizes = $this->parseJsonFile();

        unset($prizes[$id]);

        $this->writeJsonFile($prizes);
    }

    /**
     * {@inheritdoc}
     */
    public function updatePrize(array $prize)
    {
        $prizes = $this->parseJsonFile();

        if (array_key_exists($prize['id'], $prizes)) {
            $prizes[$prize['id']] = $prize;
        }

        $this->writeJsonFile($prizes);
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        return $this->parseJsonFile();
    }

    /**
     * Parse json file and return prizes as array
     *
     * @return array
     */
    private function parseJsonFile(): array
    {
        $content = file_get_contents($this->jsonFile);

        return json_decode($content, $assoc = true);
    }

    /**
     * Write prizes to json file
     *
     * @param array $prizes
     */
    private function writeJsonFile(array $prizes)
    {
        $content = json_encode($prizes);

        file_put_contents($this->jsonFile, $content);
    }
}
