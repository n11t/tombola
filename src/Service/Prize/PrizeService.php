<?php
declare(strict_types=1);

namespace N11t\Tombola\Service\Prize;

interface PrizeService
{

    /**
     * Find prize by given id.
     *
     * @param int $id
     * @return array
     * @throws PrizeNotFoundException
     */
    public function findById(int $id): array ;

    /**
     * Persists given prize.
     *
     * @param array $prize
     * @throws DuplicatePrizeException
     */
    public function addPrize(array $prize);

    /**
     * Delete the prize for given id.
     *
     * @param int $id
     */
    public function deletePrize(int $id);

    /**
     * The prize to update. Prize is the array with new values.
     *
     * @param array $prize
     */
    public function updatePrize(array $prize);

    /**
     * Find all prizes.
     *
     * @return array
     */
    public function findAll(): array;
}
