<?php
declare(strict_types=1);

namespace N11t\Tombola\Service\Prize;

class JsonBlankService implements BlankService
{

    /**
     * @var string
     */
    private $file;

    public function __construct(string $jsonFile)
    {
        $this->file = $jsonFile;
    }

    /**
     * @return array
     */
    public function getBlank(): array
    {
        $content = file_get_contents($this->file);
        $blank = json_decode($content, $assoc = true);

        return $blank;
    }

    /**
     * Update blank.
     *
     * @param array $blank
     */
    public function updateBlank(array $blank)
    {
        $content = json_encode($blank);

        file_put_contents($this->file, $content);
    }
}
