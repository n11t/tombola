<?php
declare(strict_types=1);

namespace N11t\Tombola\UseCase\Image;

use N11t\Tombola\Input\Image\GetImageInput as Input;
use N11t\Tombola\Output\Image\GetImageOutput as Output;
use N11t\Tombola\Service\Image\ImageNotFoundException;
use N11t\Tombola\Service\Image\ImageService;
use N11t\Tombola\Service\Prize\BlankService;
use N11t\Tombola\Service\Prize\PrizeNotFoundException;
use N11t\Tombola\Service\Prize\PrizeService;
use N11t\Tombola\UseCase\Image\GetImageException;

class GetImageUseCase
{

    /**
     * @var PrizeService
     */
    private $prizeService;

    /**
     * @var BlankService
     */
    private $blankService;

    /**
     * @var ImageService
     */
    private $imageService;

    public function __construct(ImageService $imageService, PrizeService $prizeService, BlankService $blankService)
    {
        $this->prizeService = $prizeService;
        $this->blankService = $blankService;
        $this->imageService = $imageService;
    }

    /**
     * @param Input $input
     * @param Output $output
     * @throws GetImageException
     */
    public function process(Input $input, Output $output)
    {
        $prize = $this->getPrize($input);
        $fileName = $this->getImageFile($prize);

        $output->setFilename($fileName);
    }

    /**
     * @param Input $input
     * @return array
     */
    private function getPrize(Input $input): array
    {
        try {
            return $this->prizeService->findById($input->getId());
        } catch (PrizeNotFoundException $exn) {
            return $this->blankService->getBlank();
        }
    }

    /**
     * @param array $prize
     * @return string
     * @throws GetImageException
     */
    private function getImageFile(array $prize): string
    {
        try {
            return $this->imageService->findImage($prize);
        } catch (ImageNotFoundException $exn) {
            throw new GetImageException($exn);
        }
    }
}
