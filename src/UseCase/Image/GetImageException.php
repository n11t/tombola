<?php
declare(strict_types=1);

namespace N11t\Tombola\UseCase\Image;

use Throwable;

class GetImageException extends \Exception
{

    public function __construct(Throwable $previous = null)
    {
        parent::__construct('Unable to get image.', 500, $previous);
    }
}
