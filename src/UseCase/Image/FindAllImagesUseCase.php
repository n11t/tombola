<?php
declare(strict_types=1);

namespace N11t\Tombola\UseCase\Image;

use N11t\Tombola\Output\Image\FindAllImagesOutput as Output;
use N11t\Tombola\Service\Image\ImageService;

class FindAllImagesUseCase
{

    /**
     * @var ImageService
     */
    private $imageService;

    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    public function process(Output $output)
    {
        $images = $this->imageService->findAll();

        $output->setImages($images);
    }
}
