<?php

namespace N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Output\Prize\FindAllPrizesOutput as Output;
use N11t\Tombola\Service\Prize\PrizeService;

class FindAllPrizesUseCase
{

    /**
     * @var PrizeService
     */
    private $prizeService;

    public function __construct(PrizeService $prizeService)
    {
        $this->prizeService = $prizeService;
    }

    /**
     * @param Output $output
     */
    public function process(Output $output)
    {
        $prizes = $this->prizeService->findAll();

        $output->setPrizes($prizes);
    }
}
