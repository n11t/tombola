<?php
declare(strict_types=1);

namespace N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Input\Prize\CreatePrizeInput as Input;
use N11t\Tombola\Output\Prize\CreatePrizeOutput as Output;
use N11t\Tombola\Service\Image\ImageNotFoundException;
use N11t\Tombola\Service\Image\ImageService;
use N11t\Tombola\Service\Prize\DuplicatePrizeException;
use N11t\Tombola\Service\Prize\PrizeNotFoundException;
use N11t\Tombola\Service\Prize\PrizeService;

class CreatePrizeUseCase
{

    /**
     * @var PrizeService
     */
    private $prizeService;

    /**
     * @var ImageService
     */
    private $imageService;

    public function __construct(PrizeService $prizeService, ImageService $imageService)
    {
        $this->prizeService = $prizeService;
        $this->imageService = $imageService;
    }

    /**
     * @param Input $input
     * @param Output $output
     */
    public function process(Input $input, Output $output)
    {
        $prize = $input->getPrize();

        if (!$this->validatePrize($output, $prize)) {
            return;
        }

        $this->addMissingAttributes($prize);

        $this->addPrize($output, $prize);
    }

    private function addMissingAttributes(array &$prize)
    {
        $prize['description'] = $prize['description'] ?? '';
    }

    /**
     * @param Output $output
     * @param array $prize
     */
    private function addPrize(Output $output, array $prize)
    {
        try {
            $this->prizeService->addPrize($prize);
        } catch (DuplicatePrizeException $exn) {
            $output->setCreatePrizeError($prize, 'Duplicate');
        }
    }

    /**
     * Validate the given prize.
     *
     * @param Output $output
     * @param array $prize
     * @return bool
     */
    private function validatePrize(Output $output, array $prize): bool
    {
        $errors = [];
        if ($prize['id'] < 1) {
            $errors[] = 'InvalidId';
        }

        if ($this->isDuplicate($prize)) {
            $errors[] = 'Duplicate';
        }

        if ($this->isImageMissing($prize)) {
            $errors[] = 'ImageNotFound';
        }

        foreach ($errors as $error) {
            $output->setCreatePrizeError($prize, $error);
        }

        return count($errors) < 1;
    }

    /**
     * Check if a prize with given id is already persisted in the service.
     *
     * @param array $prize
     * @return bool
     */
    private function isDuplicate(array $prize): bool
    {
        try {
            $this->prizeService->findById($prize['id']);
            return true;
        } catch (PrizeNotFoundException $exn) {
            return false;
        }
    }

    /**
     * Check if the image, referred by the prize, is present.
     *
     * @param array $prize
     * @return bool
     */
    private function isImageMissing(array $prize): bool
    {
        try {
            $this->imageService->findImage($prize);
            return false;
        } catch (ImageNotFoundException $exn) {
            return true;
        }
    }
}
