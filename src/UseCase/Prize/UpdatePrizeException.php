<?php
declare(strict_types=1);

namespace N11t\Tombola\UseCase\Prize;

use Throwable;

class UpdatePrizeException extends \Exception
{

    public function __construct(Throwable $previous = null)
    {
        parent::__construct('Unable to update prize', 500, $previous);
    }
}
