<?php

namespace N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Input\Prize\ValidatePrizeCSVInput as Input;
use N11t\Tombola\Output\Prize\ValidatePrizeCSVOutput as Output;
use N11t\Tombola\Service\Image\ImageNotFoundException;
use N11t\Tombola\Service\Image\ImageService;

class ValidatePrizeCSVUseCase
{

    /**
     * @var ImageService
     */
    private $imageService;

    public function __construct(ImageService $imageService)
    {
        $this->imageService = $imageService;
    }

    /**
     * @param Input $input
     * @param Output $output
     */
    public function process(Input $input, Output $output)
    {
        try {
            $rows = $this->getContent($input);
            $this->validateRows($rows);
        } catch (FileNotFoundException $exn) {
            $output->addFileError('NotFound');
        } catch (RowErrorException $exn) {
            $output->setRowErrors($exn->getErrors());
        }
    }

    /**
     * @param array $rows
     * @throws RowErrorException
     */
    public function validateRows(array $rows)
    {
        $errors = [];
        $prizes = [];
        $rowCount = 1;
        foreach ($rows as $row) {
            if (!$this->isImageAvailable($row)) {
                $errors[$rowCount] = 'ImageNotFound';
            }
            if ($this->isDuplicateNumber($row[0], $row[1], $prizes)) {
                $errors[$rowCount] = 'DuplicateNumber';
            }

            ++$rowCount;
        }

        if (\count($errors) >= 1) {
            throw new RowErrorException($errors);
        }
    }

    /**
     * @param Input $input
     * @return array
     */
    public function getContent(Input $input): array
    {
        $filePath = $input->getFilePath();

        $this->validateFile($filePath);

        $handle = \fopen($filePath, 'rb');
        $rows = [];
        while (false !== ($row = \fgetcsv($handle))) {
            $rows[] = $row;
        }
        fclose($handle);

        return $rows;
    }

    /**
     * @param string $filePath
     * @throws FileNotFoundException
     */
    private function validateFile(string $filePath)
    {
        if (!\is_file($filePath)) {
            throw new FileNotFoundException();
        }
    }

    /**
     * @param int $min
     * @param int $max
     * @param array $prizes
     * @return bool
     */
    private function isDuplicateNumber(int $min, int $max, array &$prizes): bool
    {
        for ($number = $min; $number <= $max; ++$number) {
            if (isset($prizes[$number])) {
                return true;
            }

            $prizes[$number] = true;
        }

        return false;
    }

    /**
     * @param array $row
     * @return bool
     */
    private function isImageAvailable(array $row): bool
    {
        try {
            $this->imageService->findImage([
                'image' => $row[2]
            ]);
            return true;
        } catch (ImageNotFoundException $exn) {
            return false;
        }
    }
}
