<?php
declare(strict_types=1);

namespace N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Input\Prize\UpdatePrizeInput as Input;
use N11t\Tombola\Service\Prize\PrizeNotFoundException;
use N11t\Tombola\Service\Prize\PrizeService;
use N11t\Tombola\UseCase\Prize\UpdatePrizeException;

class UpdatePrizeUseCase
{

    /**
     * @var PrizeService
     */
    private $prizeService;

    public function __construct(PrizeService $prizeService)
    {
        $this->prizeService = $prizeService;
    }

    /**
     * @param Input $input
     * @throws UpdatePrizeException
     */
    public function process(Input $input)
    {
        $prize = $this->findPrize($input->getId());
        $this->updatePrize($prize, $input);

        $this->prizeService->updatePrize($prize);
    }

    /**
     * @param int $id
     * @return array
     * @throws UpdatePrizeException
     */
    private function findPrize(int $id): array
    {
        try {
            return $this->prizeService->findById($id);
        } catch (PrizeNotFoundException $exn) {
            throw new UpdatePrizeException($exn);
        }
    }

    /**
     * @param array $prize
     * @param Input $input
     */
    private function updatePrize(array &$prize, Input $input)
    {
        $image = $input->getImage();
        if (null !== $image) {
            $prize['image'] = $image;
        }

        $description = $input->getDescription();
        if (null !== $description) {
            $prize['description'] = $description;
        }
    }
}
