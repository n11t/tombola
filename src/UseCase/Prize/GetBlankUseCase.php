<?php
declare(strict_types=1);

namespace N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Output\Prize\GetBlankOutput as Output;
use N11t\Tombola\Service\Prize\BlankService;

class GetBlankUseCase
{

    /**
     * @var BlankService
     */
    private $blankService;

    public function __construct(BlankService $blankService)
    {
        $this->blankService = $blankService;
    }

    public function process(Output $output)
    {
        $blank = $this->blankService->getBlank();

        $output->setBlank($blank);
    }
}
