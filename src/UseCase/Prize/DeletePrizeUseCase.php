<?php
declare(strict_types=1);

namespace N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Input\Prize\DeletePrizeInput as Input;
use N11t\Tombola\Service\Prize\PrizeService;

class DeletePrizeUseCase
{

    /**
     * @var PrizeService
     */
    private $prizeService;

    public function __construct(PrizeService $prizeService)
    {
        $this->prizeService = $prizeService;
    }

    public function process(Input $input)
    {
        $id = $input->getId();

        $this->prizeService->deletePrize($id);
    }
}
