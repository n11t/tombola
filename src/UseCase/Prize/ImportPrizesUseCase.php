<?php
declare(strict_types=1);

namespace N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Output\Prize\ImportPrizesOutput as Output;
use N11t\Tombola\Service\Prize\DuplicatePrizeException;
use N11t\Tombola\Service\Prize\ImportService;
use N11t\Tombola\Service\Prize\PrizeService;

class ImportPrizesUseCase
{

    /**
     * @var ImportService
     */
    private $importService;

    /**
     * @var PrizeService
     */
    private $prizeService;

    /**
     * ImportPrizeUseCase constructor.
     * @param ImportService $importService
     * @param PrizeService $prizeService
     */
    public function __construct(ImportService $importService, PrizeService $prizeService)
    {
        $this->importService = $importService;
        $this->prizeService = $prizeService;
    }

    public function process(Output $output)
    {
        $prizes = $this->importService->getPrizesToImport();

        $this->addPrizes($output, $prizes);
    }

    /**
     * @param Output $output
     * @param array $prizes
     */
    private function addPrizes(Output $output, array $prizes)
    {
        foreach ($prizes as $prize) {
            $this->addPrize($output, $prize);
        }
    }

    private function addPrize(Output $output, array $prize)
    {
        try {
            $this->prizeService->addPrize($prize);
        } catch (DuplicatePrizeException $exn) {
            $output->addError($exn->getPrize(), 'Duplicate');
        }
    }
}
