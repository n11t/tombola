<?php

namespace N11t\Tombola\UseCase\Prize;

class RowErrorException extends \Exception
{
    /**
     * @var array
     */
    private $errors;

    /**
     * RowErrorException constructor.
     * @param array $errors
     */
    public function __construct(array $errors)
    {
        $this->errors = $errors;
    }

    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
