<?php
declare(strict_types=1);

namespace N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Input\Prize\UpdateBlankInput as input;
use N11t\Tombola\Output\Prize\UpdateBlankOutput as Output;
use N11t\Tombola\Service\Image\ImageNotFoundException;
use N11t\Tombola\Service\Image\ImageService;
use N11t\Tombola\Service\Prize\BlankService;

class UpdateBlankUseCase
{

    /**
     * @var BlankService
     */
    private $blankService;

    /**
     * @var ImageService
     */
    private $imageService;

    public function __construct(BlankService $blankService, ImageService $imageService)
    {
        $this->blankService = $blankService;
        $this->imageService = $imageService;
    }

    /**
     * @param input $input
     * @param Output $output
     */
    public function process(Input $input, Output $output)
    {
        $blank = $input->getBlank();

        if (!$this->validateBlank($output, $blank)) {
            return;
        }

        $this->blankService->updateBlank($blank);
    }

    /**
     * @param Output $output
     * @param array $blank
     * @return bool
     */
    private function validateBlank(Output $output, array $blank): bool
    {
        $errors = [];

        if ($this->isImageMissing($blank)) {
            $errors[] = 'ImageNotFound';
        }

        foreach ($errors as $error) {
            $output->addBlankError($blank, $error);
        }

        return count($errors) < 1;
    }

    /**
     * Check if given blank image is missing.
     *
     * @param array $blank
     * @return bool
     */
    private function isImageMissing(array $blank): bool
    {
        try {
            $this->imageService->findImage($blank);
            return false;
        } catch (ImageNotFoundException $exn) {
            return true;
        }
    }
}
