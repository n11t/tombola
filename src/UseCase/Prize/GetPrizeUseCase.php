<?php
declare(strict_types=1);

namespace N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Input\Prize\GetPrizeInput as Input;
use N11t\Tombola\Output\Prize\GetPrizeOutput as Output;
use N11t\Tombola\Service\Prize\BlankService;
use N11t\Tombola\Service\Prize\PrizeNotFoundException;
use N11t\Tombola\Service\Prize\PrizeService;

class GetPrizeUseCase
{

    /**
     * @var PrizeService
     */
    private $prizeService;

    /**
     * @var BlankService
     */
    private $blankService;

    public function __construct(PrizeService $prizeService, BlankService $blankService)
    {
        $this->prizeService = $prizeService;
        $this->blankService = $blankService;
    }

    /**
     * @param Input $input
     * @param Output $output
     */
    public function process(Input $input, Output $output)
    {
        $prize = $this->findPrize($input);

        $output->setPrize($prize);
    }

    /**
     * @param Input $input
     * @return array
     */
    private function findPrize(Input $input): array
    {
        $id = $input->getId();

        $isPrize = false;
        try {
            $prize = $this->prizeService->findById($id);
            $isPrize = true;
        } catch (PrizeNotFoundException $exn) {
            $prize = $this->blankService->getBlank();
            $prize['id'] = $id;
        }

        $prize['isPrize'] = $isPrize;

        return $prize;
    }
}
