<?php

namespace N11t\Tombola;

use Silex\Application as SilexApplication;

class Application extends SilexApplication
{
    use SilexApplication\TwigTrait;
    use SilexApplication\UrlGeneratorTrait;
}
