<?php

namespace N11t\Tombola\Controller;

use N11t\Tombola\Application;
use Symfony\Component\HttpFoundation\Response;

class BackendController
{

    public function indexAction(Application $app): Response
    {
        return $app->render('backend/index.html.twig');
    }

    public function blankAction(Application $app): Response
    {
        return $app->render('backend/blank.html.twig');
    }
}
