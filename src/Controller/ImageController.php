<?php
declare(strict_types=1);

namespace N11t\Tombola\Controller;

use N11t\Tombola\Input\Image\SymfonyGetImageInput;
use N11t\Tombola\Output\Image\SymfonyGetImageOutput;
use Silex\Application;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ImageController
{

    public function indexAction(Request $request, Application $app): Response
    {
        $input = new SymfonyGetImageInput($request);
        $output = new SymfonyGetImageOutput();

        $app['useCase.getImage']->process($input, $output);

        return $app->sendFile(
            $output->getFileName()
        );
    }
}
