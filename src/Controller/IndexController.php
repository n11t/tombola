<?php

namespace N11t\Tombola\Controller;

use N11t\Tombola\Application;

class IndexController
{

    public function indexAction(Application $app)
    {
        return $app->render('index.html.twig');
    }
}
