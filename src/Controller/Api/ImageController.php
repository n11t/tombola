<?php
declare(strict_types=1);

namespace N11t\Tombola\Controller\Api;

use N11t\Tombola\Application;
use N11t\Tombola\Output\Image\ArrayFindAllImagesOutput;
use Symfony\Component\HttpFoundation\Response;

class ImageController
{

    public function indexAction(Application $app): Response
    {
        $output = new ArrayFindAllImagesOutput();

        $app['useCase.findAllImages']->process($output);

        $images = $output->toArray();
        sort($images);

        return $app->json([
            'images' => $images
        ]);
    }
}
