<?php

namespace N11t\Tombola\Controller\Api;

use N11t\Tombola\Application;
use N11t\Tombola\Input\Prize\StaticValidatePrizeCSVInput;
use N11t\Tombola\Output\Prize\ValidateAndImportPrizeOutput;
use N11t\Tombola\Output\Prize\ValidatePrizeCSVOutput;
use Symfony\Component\HttpFoundation\Request;

class CsvController
{

    public function importAction(Application $app, Request $request)
    {
        $csvFile = $app['dir.data'] . '/prize.csv';

        $input = new StaticValidatePrizeCSVInput($csvFile);
        $output = new ValidateAndImportPrizeOutput();

        $app['useCase.validatePrizeCSV']->process($input, $output);

        if ($this->import($output, $request)) {
            $app['factory.UseCase.importPrizes']($csvFile)->process($output);
        }

        return $app->json($output->toArray());
    }

    private function import(ValidateAndImportPrizeOutput $output, Request $request): bool
    {
        $isForceImport = $request->get('force', false);
        $hasErrors = count($output->getFileErrors()) > 0;

        return $isForceImport || !$hasErrors;
    }
}
