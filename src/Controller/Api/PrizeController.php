<?php
declare(strict_types=1);

namespace N11t\Tombola\Controller\Api;

use N11t\Tombola\Application;
use N11t\Tombola\Input\Prize\SymfonyCreatePrizeInput;
use N11t\Tombola\Input\Prize\SymfonyDeletePrizeInput;
use N11t\Tombola\Input\Prize\SymfonyGetPrizeInput;
use N11t\Tombola\Input\Prize\SymfonyUpdatePrizeInput;
use N11t\Tombola\Output\Prize\ArrayCreatePrizeOutput;
use N11t\Tombola\Output\Prize\ArrayFindAllPrizesOutput;
use N11t\Tombola\Output\Prize\SymfonyGetPrizeOutput;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PrizeController
{

    public function indexAction(Application $app): Response
    {
        $output = new ArrayFindAllPrizesOutput();

        $app['useCase.findAllPrizes']->process($output);

        $prizes = $output->toArray();
        foreach ($prizes as &$prize) {
            $this->setImageUrl($app, $prize);
        }

        return $app->json([
            'success' => true,
            'prizes' => $prizes
        ]);
    }

    /**
     * @param Request $request
     * @param Application $app
     * @return Response
     */
    public function readAction(Request $request, Application $app): Response
    {
        $input = new SymfonyGetPrizeInput($request);
        $output = new SymfonyGetPrizeOutput();

        $app['useCase.getPrize']->process($input, $output);

        $prize = $output->getPrize();

        $this->setImageUrl($app, $prize);

        return $app->json([
            'prize' => $prize
        ]);
    }

    /**
     * @param Request $request
     * @param Application $app
     * @return Response
     */
    public function createAction(Request $request, Application $app): Response
    {
        $input = new SymfonyCreatePrizeInput($request);
        $output = new ArrayCreatePrizeOutput();

        $app['useCase.createPrize']->process($input, $output);

        $data = $output->toArray();

        return $app->json([
            'success' => $output->isSuccess(),
            'data' => $data
        ]);
    }

    /**
     * @param Request $request
     * @param Application $app
     * @return Response
     */
    public function deleteAction(Request $request, Application $app): Response
    {
        $input = new SymfonyDeletePrizeInput($request);

        $app['useCase.deletePrize']->process($input);

        return $app->json([
            'success' => true,
        ]);
    }

    /**
     * @param Request $request
     * @param Application $app
     * @return Response
     */
    public function updateAction(Request $request, Application $app): Response
    {
        $input = new SymfonyUpdatePrizeInput($request);

        $app['useCase.updatePrize']->process($input);

        return $app->json([
            'success' => true
        ]);
    }

    private function setImageUrl(Application $app, array &$prize)
    {
        $prize['imageUrl'] = $app->url('image_index', [
            'id' => $prize['id'],
        ]);
    }
}
