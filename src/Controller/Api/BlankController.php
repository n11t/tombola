<?php
declare(strict_types=1);

namespace N11t\Tombola\Controller\Api;

use N11t\Tombola\Application;
use N11t\Tombola\Input\Prize\SymfonyUpdateBlankInput;
use N11t\Tombola\Output\Prize\ArrayGetBlankOutput;
use N11t\Tombola\Output\Prize\ArrayUpdateBlankOutput;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class BlankController
{

    /**
     * @param Application $app
     * @return Response
     */
    public function readAction(Application $app): Response
    {
        $output = new ArrayGetBlankOutput();

        $app['useCase.getBlank']->process($output);

        return $app->json([
            'blank' => $output->toArray()
        ]);
    }

    /**
     * @param Request $request
     * @param Application $app
     * @return Response
     */
    public function updateAction(Request $request, Application $app): Response
    {
        $input = new SymfonyUpdateBlankInput($request);
        $output = new ArrayUpdateBlankOutput();

        $app['useCase.updateBlank']->process($input, $output);

        return $app->json([
            'success' => $output->isSuccess(),
            'errors' => $output->toArray(),
        ]);
    }
}
