[![pipeline status](https://gitlab.com/n11t/tombola/badges/master/pipeline.svg)](https://gitlab.com/n11t/tombola/commits/master) [![coverage report](https://gitlab.com/n11t/tombola/badges/master/coverage.svg)](https://gitlab.com/n11t/tombola/commits/master) [Download](https://gitlab.com/n11t/tombola/-/jobs/artifacts/master/raw/tombola.tgz?job=build)

# Tombola

This is a charity project for [BÜCHERBÖRSE KÖLN E.V.](http://buecherboerse.org/).

![Tombola](/readme/tombola.png)

## Install

Just clone this repo into `/var/www/tombola/`

    git clone https://gitlab.com/n11t/tombola.git /var/www/tombola

and make sure your Apache Document-Root points to `./web/`.  

If you use NGINX please see [Silex documentation](https://silex.symfony.com/doc/2.0/web_servers.html) for webserver configuration.

## Configure

If needed, update permissions for `./app/data/prizes.json`  

## Image

Please place all images you want to use in the `./data/images/` Folder before you import or create prizes.

## Backend

To add, update or delete prizes call `/backend/` on your host. You can also change the blank settings there.  

## Import from csv

To create more than one prize, you can import an csv file.

Create a csv file (separator = `,` and enclosure = `"`) where each line is one prize-group.  
1. column is the min value.
1. column is the max value.
1. column is the image.
1. column is the description.

So if you want to create 20 prizes between 15 and 35 just add this sample line

    15,35,buch.png,"A book and a coffee" 

Use the enclosure on every column with whitespace!

Place the csv file at `./app/data/prize.csv` and call `/api/csv/import` on you host.

If you see `[]` everything is okay and the csv is imported. Check the new prizes in the `/backend`.

Otherwise you will see the rows with related errors.
**Warning Rows without errors will be imported** and appear as duplicates when you import them again.

If you create 2 lines with overlapping numbers, a Duplicate error will be shown on the second row.

    15,35,buch.png,Book
    36,40,tisch.png,"A table"
    34,34,special.png,"Special prize"

This csv file will show following error

     {
       "rows": {
         "3": "DuplicateNumber"
        }
     }
