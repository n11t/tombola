#!/bin/bash

docker-compose run -w /app php bash ./ci/phplint.sh || exit 1
docker-compose run -w /app php ./vendor/bin/phpcs --standard=PSR2 src tests || exit 1
docker-compose run -w /app php ./vendor/bin/phpunit || exit 1
