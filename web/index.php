<?php
declare(strict_types=1);

use N11t\Tombola\Application;
use N11t\Tombola\Service\Image\FileImageService;
use N11t\Tombola\Service\Prize\CSVImportService;
use N11t\Tombola\Service\Prize\CSVPrizeService;
use N11t\Tombola\Service\Prize\JsonBlankService;
use N11t\Tombola\Service\Prize\JsonPrizeService;
use N11t\Tombola\UseCase\Image\FindAllImagesUseCase;
use N11t\Tombola\UseCase\Image\GetImageUseCase;
use N11t\Tombola\UseCase\Prize\CreatePrizeUseCase;
use N11t\Tombola\UseCase\Prize\DeletePrizeUseCase;
use N11t\Tombola\UseCase\Prize\FindAllPrizesUseCase;
use N11t\Tombola\UseCase\Prize\GetBlankUseCase;
use N11t\Tombola\UseCase\Prize\GetPrizeUseCase;
use N11t\Tombola\UseCase\Prize\ImportPrizesUseCase;
use N11t\Tombola\UseCase\Prize\UpdateBlankUseCase;
use N11t\Tombola\UseCase\Prize\UpdatePrizeUseCase;
use N11t\Tombola\UseCase\Prize\ValidatePrizeCSVUseCase;

require_once __DIR__ . '/../vendor/autoload.php';

$app = new Application();

$app['debug'] = true;

$app['dir.project'] = dirname(__DIR__);
$app['dir.app']     = $app['dir.project'] . '/app';
$app['dir.data']    = $app['dir.app'] . '/data';

$app->register(new \Silex\Provider\TwigServiceProvider(), [
    'twig.path' => $app['dir.app'] . '/resources/views',
]);

$app['service.blankService'] = new JsonBlankService($app['dir.data'] . '/blank.json');
$app['service.prizeService'] = new JsonPrizeService($app['dir.data'] . '/prize.json');
$app['service.imageService'] = new FileImageService($app['dir.data'] . '/images');

$app['useCase.getPrize'] = new GetPrizeUseCase(
    $app['service.prizeService'],
    $app['service.blankService']
);
$app['useCase.getImage'] = new GetImageUseCase(
    $app['service.imageService'],
    $app['service.prizeService'],
    $app['service.blankService']
);
$app['useCase.validatePrizeCSV'] = new ValidatePrizeCSVUseCase(
    $app['service.imageService']
);
$app['useCase.createPrize'] = new CreatePrizeUseCase(
    $app['service.prizeService'],
    $app['service.imageService']
);
$app['useCase.deletePrize'] = new DeletePrizeUseCase(
    $app['service.prizeService']
);
$app['useCase.updatePrize'] = new UpdatePrizeUseCase(
    $app['service.prizeService']
);
$app['useCase.findAllPrizes'] = new FindAllPrizesUseCase(
    $app['service.prizeService']
);
$app['useCase.getBlank'] = new GetBlankUseCase(
    $app['service.blankService']
);
$app['useCase.updateBlank'] = new UpdateBlankUseCase(
    $app['service.blankService'],
    $app['service.imageService']
);
$app['useCase.findAllImages'] = new FindAllImagesUseCase(
    $app['service.imageService']
);
$app['factory.UseCase.importPrizes'] = $app->protect(function (string $csvFile) use ($app) {
    $importService = new CSVImportService($csvFile);

    return new ImportPrizesUseCase($importService, $app['service.prizeService']);
});

$app->get('/api/prize/', 'N11t\\Tombola\\Controller\\Api\\PrizeController::indexAction')
    ->bind('api_prize_index');

$app->get('/api/prize/{id}', 'N11t\\Tombola\\Controller\\Api\\PrizeController::readAction')
    ->assert('id', '\d*')
    ->bind('api_prize_read');

$app->post('/api/prize/', 'N11t\\Tombola\\Controller\\Api\\PrizeController::createAction')
    ->bind('api_prize_create');

$app->delete('/api/prize/{id}', 'N11t\\Tombola\\Controller\\Api\\PrizeController::deleteAction')
    ->assert('id', '\d*')
    ->bind('api_prize_delete');

$app->put('/api/prize/{id}', 'N11t\\Tombola\\Controller\\Api\\PrizeController::updateAction')
    ->assert('id', '\d*')
    ->bind('api_prize_update');

$app->get('/api/image/', 'N11t\\Tombola\\Controller\\Api\\ImageController::indexAction')
    ->bind('api_image_index');

$app->get('/api/csv/import', 'N11t\\Tombola\\Controller\\Api\\CsvController::importAction')
    ->bind('api_csv_import');

$app->get('/api/blank/', 'N11t\\Tombola\\Controller\\Api\\BlankController::readAction')
    ->bind('api_blank_reade');

$app->put('/api/blank/', 'N11t\\Tombola\\Controller\\Api\\BlankController::updateAction')
    ->bind('api_blank_update');

$app->get('/image/{id}', 'N11t\\Tombola\\Controller\\ImageController::indexAction')
    ->assert('id', '\d*')
    ->bind('image_index');

$app->get('/', 'N11t\\Tombola\\Controller\\IndexController::indexAction')
    ->bind('index_index');

$app->get('/backend/', 'N11t\\Tombola\\Controller\\BackendController::indexAction')
    ->bind('backend_index');

$app->get('/backend/blank', 'N11t\\Tombola\\Controller\\BackendController::blankAction')
    ->bind('backend_blank');

$app->run();
