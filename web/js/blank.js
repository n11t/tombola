/**
 * Blank
 *
 * @typedef {Object} Blank
 *
 * @property {string} image
 * @property {string} description
 */

(function ($) {
    const $selectImage = $('select[name=blank-image]');
    const $inputDescription = $('input[name=blank-description]');
    const $buttonSave = $('#blan-save');
    const $alerts = $('#alerts');

    // LISTENER --------------------------------------------------------------------
    $buttonSave.on('click', function (event) {
        let blank = {
            image: $selectImage.val(),
            description: $inputDescription.val()
        };

        updateBlank(blank, function (result) {
            if (result.success) {
                showSuccess('Blank updated');
            } else {
                showError(result.errors.join(', '));
            }
        })
    });

    // MAIN ------------------------------------------------------------------------
    function main() {
        updateImages();
        updateForm();
    }

    // FUNCTIONS -------------------------------------------------------------------
    function updateImages() {
        getImages(function (images) {
            $selectImage.find('option').remove();

            appendOption($selectImage, '', '-- Please Select --');
            $.each(images, function(id, image) {
                appendOption($selectImage, image, image);
            });
        });
    }

    function updateForm() {
        getBlank(function (blank) {
            $('select[name=blank-image] option[value="' + blank.image + '"]').prop('selected', true);
            $inputDescription.val(blank.description);
        });
    }

    function showError(msg) {
        showMessage('danger', msg);
    }

    function showSuccess(msg) {
        showMessage('success', msg);
    }

    function showMessage(type, msg) {
        let html = `
<div class="alert alert-${type} alert-dismissible fade show" role="alert">
    ${msg}
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
</div>`;

        $alerts.html(html);
    }

    function appendOption($select, value, text) {
        $select.append($('<option>', {
            value: value,
            text: text
        }));
    }

    // API -------------------------------------------------------------------------
    function getImages(callback) {
        $.get({
            url: "/api/image/"
        }).done(function (result) {
            callback(result.images);
        });
    }

    /**
     * @param {Blank} blank
     * @param callback
     */
    function updateBlank(blank, callback) {
        $.ajax({
            method: 'PUT',
            data: blank,
            url: '/api/blank/'
        }).done(function (result) {
            callback(result)
        });
    }

    function getBlank(callback) {
        $.get({
            url: "/api/blank/"
        }).done(function (result) {
            callback(result.blank);
        });
    }

    main();
}(jQuery));
