/**
 * Prize
 *
 * @typedef {Object} Prize
 *
 * @property {number} id
 * @property {string} image
 * @property {string} description
 * @property {boolean} isPrize
 * @property {string} imageUrl
 * @property {boolean} paidOff
 */

(function ($) {
    const $tableBody = $('.table-prizes-body');
    const $inputId = $('input[name=id]');
    const $selectImage = $('select[name=image]');
    const $inputDescription = $('input[name=description]');
    const $formErrors = $('span.prize-errors');
    const $prizeModal = $('#prizeModal');
    const $buttonPrizeSave = $('button.modal-prize-save');

    // LISTENER --------------------------------------------------------
    $tableBody.on('click', '.prize-delete', function() {
        let id = $(this).data('prize-id');

        if (confirm("Delete prize with id '" + id + "'")) {
            deletePrize(id);
        }
    });

    $tableBody.on('click', '.prize-update', function () {
        let id = $(this).data('prize-id');

        $prizeModal.data('prize-id', id);
        $prizeModal.modal('show');
    });

    $(document).on('click', 'button.prize-create', function () {
        let prize = {
            id: $inputId.val(),
            image: $selectImage.val(),
            description: $inputDescription.val()
        };

        createPrize(prize);
    });

    $buttonPrizeSave.on('click', function() {
        let id = $prizeModal.data('prize-id');

        if (id) {
            let image = $prizeModal.find('#modal-prize-image').val();
            let description = $prizeModal.find('#modal-prize-description').val();

            console.log(id, {
                image: image,
                description: description
            });

            updatePrize(id, {
                image: image,
                description: description
            });

            $prizeModal.modal('hide');
            main();
        }

        return true;
    });

    $prizeModal.on('show.bs.modal', function (event) {
        let $modal = $(this);

        updateModal($modal);
    });

    // MAIN --------------------------------------------------------
    function main() {
        updatePrizes();
        updateImages();
        resetForm();
    }

    // FUNCTIONS --------------------------------------------------------
    function resetForm() {
        $formErrors.html('');
        $inputId.val(1);
        $selectImage.val($selectImage.find("option:first").val());
        $inputDescription.val('');
    }

    function updateModal($modal) {
        let id = $modal.data('prize-id');

        getImages(function (images) {
            let $modalSelectImages = $modal.find('#modal-prize-image');
            $modalSelectImages.html('');

            $.each(images, function (id, image) {
                appendOption($modalSelectImages, image, image);
            });
        });
        getPrize(id, function (prize) {
            $modal.find('#modal-prize-id').val(prize.id);
            $modal.find('#modal-prize-description').val(prize.description);
            $modal.find('#modal-prize-image option[value="' + prize.image + '"]').prop('selected', true);
        });
    }

    function updateImages() {
        $.get({
            url: "/api/image/"
        }).done(function (result) {
            renderImages(result.images);
        });
    }

    function renderImages(images) {
        $selectImage.find('option').remove();

        appendOption($selectImage, '', '-- Please Select --');
        $.each(images, function(id, image) {
            appendOption($selectImage, image, image);
        });
    }

    function appendOption($select, value, text) {
        $select.append($('<option>', {
            value: value,
            text: text
        }));
    }

    function updatePrizes() {
        $.get({
            url: "/api/prize/"
        }).done(function (result) {
            renderPrizes(result.prizes);
        });
    }

    function renderPrizes(prizes) {
        let html = '';
        $.each(prizes, function (id, prize) {
            html += convertPrizeToTableRow(prize);
        });

        $tableBody.html(html);
    }

    /**
     * @param {Prize} prize
     */
    function convertPrizeToTableRow(prize) {
        const spanClass = prize.paidOff ? 'prize-used' : '';

        return `
        <tr data-prize-id="${prize.id}">
            <td><span class="${spanClass}">${prize.id}</span></td>
            <td><span class="${spanClass}">${prize.image}</span></td>
            <td><span class="${spanClass}">${prize.description}</span></td>
            <td>
                <i class="fa fa-trash-o prize-delete cursor-pointer" data-prize-id="${prize.id}" aria-hidden="true"></i>
                <i class="fa fa-pencil-square-o prize-update cursor-pointer" data-prize-id="${prize.id}" aria-hidden="true"></i>
            </td>
        </tr>`
    }

    function getImages(callback) {
        $.get({
            url: "/api/image/"
        }).done(function (result) {
            callback(result.images);
        });
    }

    function deletePrize(id) {
        $.ajax({
            method: 'DELETE',
            url: "/api/prize/" + id
        }).done(function () {
            main();
        });
    }

    function getPrize(id, callback) {
        $.get({
            url: '/api/prize/' + id
        }).done(function (result) {
            callback(result.prize);
        });
    }

    function createPrize(prize) {
        $.ajax({
            method: "POST",
            url: '/api/prize/',
            data: prize
        }).done(function (result) {
            if (result.success) {
                main();
            } else {
                $('span.prize-errors').html(result.data.errors.join(','));
            }
        })
    }

    function updatePrize(id, prize) {
        $.ajax({
            method: 'PUT',
            data: prize,
            url: '/api/prize/' + id
        });
    }

    main();
}(jQuery));
