/**
 * Prize
 *
 * @typedef {Object} Prize
 *
 * @property {number} id
 * @property {string} image
 * @property {string} description
 * @property {boolean} isPrize
 * @property {string} imageUrl
 */

(function ($) {
    const $searchButton = $('#tombola-submit');
    const $input = $('#tombola-input');
    const $prize = $('#prize');

    $input.focus();

    $input.keypress(function (e) {
        if (e.key === "Enter") {
            sendTicketNumber();
        }
    });
    $searchButton.on('click', sendTicketNumber());

    function sendTicketNumber() {
        const id = $input.val();

        if(!$.isNumeric(id)) {
            return;
        }

        $.get({
            url: "/api/prize/" + id
        }).done(function (result) {
            renderPrize(result.prize);
            $input.val('');
            $input.focus();
        });
    }

    /**
     * @param {Prize} prize
     */
    function renderPrize(prize) {
        $prize.empty();
        let html = '';

        if (prize.isPrize) {
            html += ``;
        }

        const bannerMessage = prize.isPrize && "Herzlichen Glückwunsch!" || "Schade :(";
        const prizeMessage = `<span class="badge badge-dark">${prize.id}</span> ${prize.description}`;

        html += `
<div class="card" style="width: 80vw;">
  <div class="card-body">
    <div class="card-title"><h1 class="ribbon"><strong class="ribbon-content"><i class="fa fa-trophy"></i>${bannerMessage}</strong></h1></div>
    <div class="prize">
        <img class="card-img prize-image zoom-in" src="${prize.imageUrl}" alt="${prize.image}">
        <p class="card-text">${prizeMessage}</p>
    </div>
  </div>
</div>`;


        $prize.html(html);
    }
}(jQuery));
