<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Input\Prize;

use N11t\Tombola\Input\Prize\CreatePrizeInput;
use Tests\N11t\Tombola\Input\TestInput;

class FakeCreatePrizeInput extends TestInput implements CreatePrizeInput
{

    /**
     * @var array
     */
    public $prize;

    /**
     * Get the prize to create.
     *
     * @return array
     */
    public function getPrize(): array
    {
        return $this->prize;
    }
}
