<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Input\Prize;

use N11t\Tombola\Input\Prize\DeletePrizeInput;
use Tests\N11t\Tombola\Input\TestInput;

class FaKeDeletePrizeInput extends TestInput implements DeletePrizeInput
{

    /**
     * @var int
     */
    public $id;

    /**
     * Return the id to delete the prize.
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
