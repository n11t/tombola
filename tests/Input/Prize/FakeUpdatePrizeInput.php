<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Input\Prize;

use N11t\Tombola\Input\Prize\UpdatePrizeInput;
use Tests\N11t\Tombola\Input\TestInput;

class FakeUpdatePrizeInput extends TestInput implements UpdatePrizeInput
{

    /**
     * @var int
     */
    public $id;

    /**
     * @var string|null
     */
    public $description;

    /**
     * @var string|null
     */
    public $image;

    /**
     * Get the id to identify with prize to update
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * The description to set.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * The image to set.
     *
     * @return string|null
     */
    public function getImage()
    {
        return $this->image;
    }
}
