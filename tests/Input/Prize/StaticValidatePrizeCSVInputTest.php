<?php

namespace Tests\N11t\Tombola\Input\Prize;

use N11t\Tombola\Input\Prize\StaticValidatePrizeCSVInput;
use PHPUnit\Framework\TestCase;

class StaticValidatePrizeCSVInputTest extends TestCase
{

    public function test()
    {
        // Arrange
        $file = '/tmp/prizes.csv';

        // Act
        $input = new StaticValidatePrizeCSVInput($file);

        // Assert
        self::assertSame($file, $input->getFilePath());
    }
}
