<?php

namespace Tests\N11t\Tombola\Input\Prize;

use N11t\Tombola\Input\Prize\ValidatePrizeCSVInput;

class FakeValidatePrizeCSVInput implements ValidatePrizeCSVInput
{

    /**
     * @var string
     */
    private $filePath;

    public function __construct(string $filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * Return the file path.
     *
     * @return string
     */
    public function getFilePath(): string
    {
        return $this->filePath;
    }
}
