<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Input\Prize;

use N11t\Tombola\Input\Prize\SymfonyGetPrizeInput;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Input\SymfonyRequestBuilder;

class SymfonyGetPrizeInputTest extends TestCase
{

    public function test()
    {
        // Arrange
        $request = SymfonyRequestBuilder::create()
            ->setAttributes([
                'id' => 12
            ])
            ->build();

        // Act
        $input = new SymfonyGetPrizeInput($request);

        // Assert
        self::assertSame(12, $input->getId());
    }
}
