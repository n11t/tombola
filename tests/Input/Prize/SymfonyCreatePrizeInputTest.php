<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Input\Prize;

use N11t\Tombola\Input\Prize\SymfonyCreatePrizeInput;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Input\SymfonyRequestBuilder;

class SymfonyCreatePrizeInputTest extends TestCase
{

    public function test()
    {
        // Arrange
        $request = SymfonyRequestBuilder::create()
            ->setRequest([
                'id' => 20,
                'image' => 'image.png',
                'description' => 'a table',
            ])
            ->build();

        // Act
        $input = new SymfonyCreatePrizeInput($request);
        $prize = $input->getPrize();

        // Assert
        self::assertEquals([
            'id' => 20,
            'image' => 'image.png',
            'description' => 'a table',
        ], $prize);
    }
}
