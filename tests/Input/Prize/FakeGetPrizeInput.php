<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Input\Prize;

use N11t\Tombola\Input\Prize\GetPrizeInput;
use Tests\N11t\Tombola\Input\TestInput;

class FakeGetPrizeInput extends TestInput implements GetPrizeInput
{

    /**
     * @var int
     */
    public $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
