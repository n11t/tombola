<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Input\Prize;

use N11t\Tombola\Input\Prize\UpdateBlankInput;
use Tests\N11t\Tombola\Input\TestInput;

class FakeUpdateBlankInput extends TestInput implements UpdateBlankInput
{

    /**
     * @var array
     */
    public $blank;

    /**
     * Get the blank to update.
     *
     * @return array
     */
    public function getBlank(): array
    {
        return $this->blank;
    }
}
