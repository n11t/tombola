<?php

namespace Tests\N11t\Tombola\Input\Prize;

use N11t\Tombola\Input\Prize\SymfonyUpdatePrizeInput;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Input\SymfonyRequestBuilder;

class SymfonyUpdatePrizeInputTest extends TestCase
{

    public function test()
    {
        // Assert
        $request = SymfonyRequestBuilder::create()
            ->setRequest([
                'image' => 'buch.jpg',
                'description' => 'A book'
            ])
            ->setAttributes([
                'id' => 10
            ])
            ->build();

        // Act
        $input = new SymfonyUpdatePrizeInput($request);

        // Assert
        self::assertSame(10, $input->getId());
        self::assertSame('buch.jpg', $input->getImage());
        self::assertSame('A book', $input->getDescription());
    }

    public function testCanHandleDefaults()
    {
        // Assert
        $request = SymfonyRequestBuilder::create()
            ->setAttributes([
                'id' => 10
            ])
            ->build();

        // Act
        $input = new SymfonyUpdatePrizeInput($request);

        // Assert
        self::assertSame(10, $input->getId());
        self::assertSame('', $input->getImage());
        self::assertSame('', $input->getDescription());
    }
}
