<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Input\Prize;

use N11t\Tombola\Input\Prize\SymfonyUpdateBlankInput;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Input\SymfonyRequestBuilder;

class SymfonyUpdateBlankInputTest extends TestCase
{

    public function test()
    {
        // Arrange
        $request = SymfonyRequestBuilder::create()
            ->setRequest([
                'image' => 'blank.png',
                'description' => 'HelloWorld'
            ])
            ->build();

        // Act
        $input = new SymfonyUpdateBlankInput($request);

        // Assert
        self::assertSame([
            'image' => 'blank.png',
            'description' => 'HelloWorld'
        ], $input->getBlank());
    }

    public function testCanHandleDefaults()
    {
        // Arrange
        $request = SymfonyRequestBuilder::create()->build();

        // Act
        $input = new SymfonyUpdateBlankInput($request);

        // Assert
        self::assertSame([
            'image' => '',
            'description' => ''
        ], $input->getBlank());
    }
}
