<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Input;

class TestInput
{

    public function __construct(array $params = [])
    {
        foreach ($params as $property => $value) {
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }
    }
}
