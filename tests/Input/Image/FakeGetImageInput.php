<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Input\Image;

use N11t\Tombola\Input\Image\GetImageInput;
use Tests\N11t\Tombola\Input\TestInput;

class FakeGetImageInput extends TestInput implements GetImageInput
{

    /**
     * @var int
     */
    public $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
}
