<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Input\Image;

use N11t\Tombola\Input\Image\SymfonyGetImageInput;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Input\SymfonyRequestBuilder;

class SymfonyGetImageInputTest extends TestCase
{

    public function test()
    {
        // Arrange
        $request = SymfonyRequestBuilder::create()
            ->setAttributes([
                'id' => 10
            ])
            ->build();

        // Act
        $input = new SymfonyGetImageInput($request);

        // Assert
        self::assertSame(10, $input->getId());
    }
}
