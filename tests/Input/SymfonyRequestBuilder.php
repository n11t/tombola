<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Input;

use Symfony\Component\HttpFoundation\Request;

class SymfonyRequestBuilder
{

    /**
     * @var array
     */
    private $query;

    /**
     * @var array
     */
    private $request;

    /**
     * @var array
     */
    private $attributes;

    /**
     * @var array
     */
    private $cookies;

    /**
     * @var array
     */
    private $files;

    /**
     * @var array
     */
    private $server;

    /**
     * @var array
     */
    private $content;

    /**
     * SymfonyRequestBuilder constructor.
     */
    private function __construct()
    {
        $this->query = [];
        $this->request = [];
        $this->attributes = [];
        $this->cookies = [];
        $this->files = [];
        $this->server = [];
        $this->content = [];
    }

    /**
     * Static method to chain
     *
     * @return SymfonyRequestBuilder
     */
    public static function create(): SymfonyRequestBuilder
    {
        return new self();
    }

    /**
     * @param array $query
     * @return $this
     */
    public function setQuery(array $query)
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @param array $request
     * @return $this
     */
    public function setRequest(array $request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @param array $attributes
     * @return $this
     */
    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * @param array $cookies
     * @return $this
     */
    public function setCookies(array $cookies)
    {
        $this->cookies = $cookies;

        return $this;
    }

    /**
     * @param array $files
     * @return $this
     */
    public function setFiles(array $files)
    {
        $this->files = $files;

        return $this;
    }

    /**
     * @param array $server
     * @return $this
     */
    public function setServer(array $server)
    {
        $this->server = $server;

        return $this;
    }

    /**
     * @param array $content
     * @return $this
     */
    public function setContent(array $content)
    {
        $this->content = $content;

        return $this;
    }

    public function build()
    {
        return new Request(
            $this->query,
            $this->request,
            $this->attributes,
            $this->cookies,
            $this->files,
            $this->server,
            $this->content
        );
    }
}
