<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Input\Prize\GetPrizeInput;
use N11t\Tombola\UseCase\Prize\GetPrizeUseCase;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Input\Prize\FakeGetPrizeInput;
use Tests\N11t\Tombola\Output\Prize\FakeGetPrizeOutput;
use Tests\N11t\Tombola\Service\Prize\FakeBlankService;
use Tests\N11t\Tombola\Service\Prize\FakePrizeService;

class GetPrizeUseCaseTest extends TestCase
{

    public function testCanFindPrize()
    {
        // Arrange
        $prize = [
            'id' => 10,
            'image' => 'buch.jpg',
            'description' => 'Hello World'
        ];

        // Act
        $output = $this->processPrize($prize);

        // Assert
        self::assertSame($prize['id'], $output->prize['id']);
        self::assertSame($prize['image'], $output->prize['image']);
        self::assertSame($prize['description'], $output->prize['description']);
    }

    public function testCanHandleBlank()
    {
        // Act
        $output = $this->processBlank();

        // Assert
        $blank = FakeBlankService::defaults();
        self::assertSame(10, $output->prize['id']);
        self::assertSame($blank['image'], $output->prize['image']);
        self::assertSame($blank['description'], $output->prize['description']);
    }

    public function testAddFlagIsPrize()
    {
        // Act
        $outputBlank = $this->processBlank();
        $outputPrize = $this->processPrize([
            'id' => 10,
            'image' => 'buch.jpg',
            'description' => 'Hello World'
        ]);

        // Assert
        self::assertFalse($outputBlank->prize['isPrize']);
        self::assertTrue($outputPrize->prize['isPrize']);
    }

    /**
     * Process the usecase with default blank values.
     *
     * @return FakeGetPrizeOutput
     */
    public function processBlank(): FakeGetPrizeOutput
    {
        // Arrange
        $input = new FakeGetPrizeInput([
            'id' => 10
        ]);

        // Act
        return $this->process($input);
    }

    /**
     * Prozess the {@see GetPrizeUseCase} with given prize
     *
     * @param array $prize
     * @return FakeGetPrizeOutput
     */
    private function processPrize(array $prize): FakeGetPrizeOutput
    {
        // Arrange
        $input = new FakeGetPrizeInput([
            'id' => $prize['id']
        ]);

        // Act
        return $this->process($input, [$prize]);
    }

    /**
     * @param GetPrizeInput $input
     * @param array $prizes
     * @return FakeGetPrizeOutput
     */
    public function process(
        GetPrizeInput $input,
        array $prizes = []
    ): FakeGetPrizeOutput {
        $prizeService = new FakePrizeService($prizes);
        $blankService = new FakeBlankService();

        $useCase = new GetPrizeUseCase($prizeService, $blankService);

        $output = new FakeGetPrizeOutput();

        $useCase->process($input, $output);

        return $output;
    }
}
