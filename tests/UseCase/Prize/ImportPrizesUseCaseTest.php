<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\UseCase\Prize;

use N11t\Tombola\UseCase\Prize\ImportPrizesUseCase;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Output\Prize\FakeImportPrizesOutput;
use Tests\N11t\Tombola\Service\Prize\FakeImportService;
use Tests\N11t\Tombola\Service\Prize\FakePrizeService;

class ImportPrizesUseCaseTest extends TestCase
{

    public function testCanImportPrizes()
    {
        // Arrange
        $service = new FakePrizeService();

        // Act
        $this->processNPrizes(10, $service);

        // Assert
        self::assertCount(10, $service->prizes);
    }

    public function testCanHandleDuplicates()
    {
        // Arrange
        $prizes = $this->generatePrizes(3);
        $duplicatePrize = $prizes[0];

        $service = new FakePrizeService([$duplicatePrize]);

        // Act
        $output = $this->process($service, $prizes);

        // Assert
        $errors = $output->errors;
        self::assertCount(1, $errors);
        self::assertSame('Duplicate', $errors[$duplicatePrize['id']]);
        self::assertCount(3, $service->prizes);
    }

    /**
     * @param int $count
     * @param FakePrizeService $prizeService
     */
    public function processNPrizes(int $count, FakePrizeService $prizeService)
    {
        $prizes = $this->generatePrizes($count);

        $this->process($prizeService, $prizes);
    }

    /**
     * @param int $count
     * @param int $start
     * @return array
     */
    public function generatePrizes(int $count, int $start = 1000): array
    {
        $min = $start;
        $max = $start + $count;
        $prizes = [];

        for ($i = $min; $i < $max; ++$i) {
            $prizes[] = [
                'id' => $i,
                'image' => "image_$i.jpg",
                'description' => "Prize $i"
            ];
        }

        return $prizes;
    }

    /**
     * @param array $prizes
     * @param FakePrizeService $prizeService
     * @return FakeImportPrizesOutput
     */
    public function process(FakePrizeService $prizeService, array $prizes = []): FakeImportPrizesOutput
    {
        $importService = new FakeImportService($prizes);

        $useCase = new ImportPrizesUseCase($importService, $prizeService);

        $output = new FakeImportPrizesOutput();

        $useCase->process($output);

        return $output;
    }
}
