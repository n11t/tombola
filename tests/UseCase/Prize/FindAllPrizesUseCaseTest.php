<?php

namespace Tests\N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Service\Prize\PrizeService;
use N11t\Tombola\UseCase\Prize\FindAllPrizesUseCase;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Output\Prize\FakeFindAllPrizesOutput;
use Tests\N11t\Tombola\Service\Prize\FakePrizeService;

class FindAllPrizesUseCaseTest extends TestCase
{

    public function testCanFindAllPrizes()
    {
        // Arrange
        $service = $this->createNPrizes(10);

        // Act
        $output = $this->process($service);

        // Assert
        self::assertCount(10, $output->prizes);
    }

    /**
     * @param int $count
     * @param int $start
     * @return FakePrizeService
     */
    public function createNPrizes(int $count, $start = 100): FakePrizeService
    {
        $prizes = [];
        $min = $start;
        $max = $start + $count;
        for ($i = $min; $i < $max; $i++) {
            $prizes[$i] = [
                'id' => $i,
                'image' => "image_$i.jpg",
                'description' => "Test_$i"
            ];
        }

        return new FakePrizeService($prizes);
    }

    /**
     * @param PrizeService $service
     * @return FakeFindAllPrizesOutput
     */
    private function process(PrizeService $service)
    {
        $useCase = new FindAllPrizesUseCase($service);

        $output = new FakeFindAllPrizesOutput();

        $useCase->process($output);

        return $output;
    }
}
