<?php

namespace Tests\N11t\Tombola\UseCase\Prize;

use N11t\Tombola\UseCase\Prize\ValidatePrizeCSVUseCase;
use Tests\N11t\Tombola\Input\Prize\FakeValidatePrizeCSVInput;
use Tests\N11t\Tombola\Output\Prize\FakeValidatePrizeCSVOutput;
use Tests\N11t\Tombola\Service\DirectoryBaseServiceTestCase;
use Tests\N11t\Tombola\Service\Image\FakeImageService;

class ValidatePrizeCSVUseCaseTest extends DirectoryBaseServiceTestCase
{

    public function testCanHandleEmptyCSVFile()
    {
        // Arrange
        $this->putCSVFile();

        // Act
        $output = $this->process();

        // Assert
        self::assertCount(0, $output->errors);
    }

    public function testCanHandleMissingCSVFile()
    {
        // Act
        $output = $this->process();

        // Assert
        $errors = $output->errors;
        self::assertSame(['NotFound'], $errors['file']);
    }

    public function testCanMarkDuplicateIdsAsError()
    {
        // Arrange
        $this->putCSVFile([
            ['100', '105', 'tisch1.png', 'A Table #1'],
            ['104', '106', 'tisch2.png', 'A Table #2']
        ]);

        // Act
        $output = $this->process();

        // Assert
        $errors = $output->errors;
        self::assertSame([2 => 'DuplicateNumber'], $errors['rows']);
    }

    public function testCanHandleMissingImage()
    {
        // Arrange
        $this->putCSVFile([
            ['100', '105', 'tisch.png', 'A Table'],
        ]);

        // Act
        $output = $this->process($throwFileNotFoundException = true);

        // Assert
        $errors = $output->errors;
        self::assertSame([1 => 'ImageNotFound'], $errors['rows']);
    }

    /**
     * @param bool $throwFileNotFoundException
     * @return FakeValidatePrizeCSVOutput
     */
    public function process(bool $throwFileNotFoundException = false): FakeValidatePrizeCSVOutput
    {
        $input = new FakeValidatePrizeCSVInput(
            $this->getCSVFilePath()
        );
        $output = new FakeValidatePrizeCSVOutput();
        $imageService = new FakeImageService('/tmp', $throwFileNotFoundException);

        $useCase = new ValidatePrizeCSVUseCase($imageService);

        $useCase->process($input, $output);

        return $output;
    }

    /**
     * Return the csv file name
     *
     * @return string
     */
    public function getCSVFilePath(): string
    {
        return $this->getUnitTestDirectory() . '/prizes.csv';
    }

    /**
     * Create csv with given rows.
     * @param array $rows
     */
    public function putCSVFile(array $rows = [])
    {
        $file = $this->getCSVFilePath();

        $handle = fopen($file, 'wb');
        foreach ($rows as $row) {
            fputcsv($handle, $row);
        }
        fclose($handle);
    }
}
