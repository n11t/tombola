<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Input\Prize\DeletePrizeInput;
use N11t\Tombola\Service\Prize\PrizeService;
use N11t\Tombola\UseCase\Prize\DeletePrizeUseCase;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Input\Prize\FaKeDeletePrizeInput;
use Tests\N11t\Tombola\Service\Prize\FakePrizeService;

class DeletePrizeUseCaseTest extends TestCase
{

    public function testCanDeletePrize()
    {
        // Arrange
        $prize = [
            'id' => 1005,
            'image' => 'book.jpg',
            'description' => 'a book'
        ];
        $service = new FakePrizeService([$prize]);
        $input = new FaKeDeletePrizeInput([
            'id' => 1005
        ]);

        // Act
        $this->process($input, $service);

        // Assert
        self::assertCount(0, $service->prizes);
    }

    public function process(DeletePrizeInput $input, PrizeService $prizeService)
    {
        $useCase = new DeletePrizeUseCase($prizeService);

        $useCase->process($input);
    }
}
