<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\UseCase\Prize;

use N11t\Tombola\UseCase\Prize\GetBlankUseCase;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Output\Prize\FakeGetBlankOutput;
use Tests\N11t\Tombola\Service\Prize\FakeBlankService;

class GetBlankUseCaseTest extends TestCase
{

    public function testCanGetBlank()
    {
        // Arrange
        $blankService = new FakeBlankService();

        $output = new FakeGetBlankOutput();

        $useCase = new GetBlankUseCase($blankService);

        // Act
        $useCase->process($output);

        // Assert
        self::assertSame(FakeBlankService::defaults(), $output->blank);
    }
}
