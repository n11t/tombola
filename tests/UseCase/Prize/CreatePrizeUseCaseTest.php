<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Input\Prize\CreatePrizeInput;
use N11t\Tombola\Service\Prize\PrizeService;
use N11t\Tombola\UseCase\Prize\CreatePrizeUseCase;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Input\Prize\FakeCreatePrizeInput;
use Tests\N11t\Tombola\Output\Prize\FakeCreatePrizeOutput;
use Tests\N11t\Tombola\Service\Image\FakeImageService;
use Tests\N11t\Tombola\Service\Prize\FakePrizeService;

class CreatePrizeUseCaseTest extends TestCase
{

    public function testCanCreatePrize()
    {
        // Arrange
        $prize = [
            'id' => 10,
            'image' => 'tisch.jpg',
            'description' => 'A table'
        ];
        $input = new FakeCreatePrizeInput([
            'prize' => $prize
        ]);
        $prizeService = new FakePrizeService();

        // Act
        $this->process($input, $prizeService);

        // Assert
        self::assertCount(1, $prizeService->prizes);
    }

    public function testCanHandleDuplicatePrize()
    {
        // Arrange
        $duplicatePrize = [
            'id' => 10,
            'image' => 'tisch.jpg',
            'description' => 'A table'
        ];
        $input = new FakeCreatePrizeInput([
            'prize' => $duplicatePrize
        ]);
        $prizeService = new FakePrizeService([$duplicatePrize]);

        // Act
        $output = $this->process($input, $prizeService);

        // Assert
        $errors = $output->errors;
        self::assertSame(['Duplicate'], $errors);
    }

    public function testCanAddMissingAttributes()
    {
        // Arrange
        $prize = [
            'id' => 10
        ];
        $input = new FakeCreatePrizeInput([
            'prize' => $prize
        ]);
        $prizeService = new FakePrizeService();

        // Act
        $output = $this->process($input, $prizeService);

        // Assert
        self::assertCount(0, $output->errors);
        self::assertSame([
            10 => [
                'id' => 10,
                'description' => ''
            ]
        ], $prizeService->prizes);
    }

    public function dataProviderInvalidIds()
    {
        return [
            [-1, 'InvalidId'],
            [0, 'InvalidId'],
            [-29, 'InvalidId'],
        ];
    }

    /**
     * @dataProvider dataProviderInvalidIds
     *
     * @param $id
     * @param string $error
     */
    public function testCanHandleInvalidIds($id, string $error)
    {
        // Arrange
        $prize = [
            'id' => $id,
            'image' => 'tisch.jpg',
            'description' =>  'A table'
        ];
        $input = new FakeCreatePrizeInput([
            'prize' => $prize
        ]);
        $prizeService = new FakePrizeService();

        // Act
        $output = $this->process($input, $prizeService);

        // Assert
        $errors = $output->errors;
        self::assertSame([$error], $errors);
        self::assertCount(0, $prizeService->prizes);
    }

    public function testValidateImageNotFound()
    {
        // Arrange
        $input = new FakeCreatePrizeInput([
            'prize' => [
                'id' => 1,
                'image' => 'tisch.png',
                'description' => 'A table'
            ]
        ]);
        $prizeService = new FakePrizeService();

        // Act
        $output = $this->process($input, $prizeService, $throwException = true);

        // Assert
        $errors = $output->errors;
        self::assertSame(['ImageNotFound'], $errors);
        self::assertCount(0, $prizeService->prizes);
    }

    /**
     * @param CreatePrizeInput $input
     * @param PrizeService $prizeService
     * @param bool $throwImageNotFoundException
     * @return FakeCreatePrizeOutput
     */
    public function process(
        CreatePrizeInput $input,
        PrizeService $prizeService,
        bool $throwImageNotFoundException = false
    ): FakeCreatePrizeOutput {
        $imageService = new FakeImageService('/tmp', $throwImageNotFoundException);
        $useCase = new CreatePrizeUseCase($prizeService, $imageService);
        $output = new FakeCreatePrizeOutput();

        $useCase->process($input, $output);

        return $output;
    }
}
