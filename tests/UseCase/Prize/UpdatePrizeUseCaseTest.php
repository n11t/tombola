<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Input\Prize\UpdatePrizeInput;
use N11t\Tombola\Service\Prize\PrizeNotFoundException;
use N11t\Tombola\Service\Prize\PrizeService;
use N11t\Tombola\UseCase\Prize\UpdatePrizeException;
use N11t\Tombola\UseCase\Prize\UpdatePrizeUseCase;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Input\Prize\FakeUpdatePrizeInput;
use Tests\N11t\Tombola\Service\Prize\FakePrizeService;

class UpdatePrizeUseCaseTest extends TestCase
{

    public function testCanUpdatePrize()
    {
        // Arrange
        $prize = [
            'id' => 40,
            'image' => 'tisch.jpg',
            'description' => 'A table'
        ];
        $service = new FakePrizeService([$prize]);
        $input = new FakeUpdatePrizeInput([
            'id' => 40,
            'image' => 'buch.jpg',
            'description' => 'A book'
        ]);

        // Act
        $this->process($input, $service);

        // Assert
        self::assertCount(1, $service->prizes);
        $actualPrize = $service->prizes[40];
        self::assertSame([
            'id' => 40,
            'image' => 'buch.jpg',
            'description' => 'A book'
        ], $actualPrize);
    }

    public function testCanHandlePrizeNotFound()
    {
        // Arrange
        $service = new FakePrizeService();
        $input = new FakeUpdatePrizeInput([
            'id' => 40,
        ]);

        // Act
        $exception = null;
        try {
            $this->process($input, $service);
        } catch (UpdatePrizeException $exn) {
            $exception = $exn;
        }

        // Assert
        self::assertInstanceOf(PrizeNotFoundException::class, $exception->getPrevious());
    }

    public function testCanIgnoreImageAndDescription()
    {
        // Arrange
        $oldPrize = [
            'id' => 40,
            'image' => 'tisch.jpg',
            'description' => 'A table'
        ];
        $service = new FakePrizeService([$oldPrize]);
        $input = new FakeUpdatePrizeInput([
            'id' => 40,
        ]);

        // Act
        $this->process($input, $service);

        // Assert
        self::assertCount(1, $service->prizes);
        $actualPrize = $service->prizes[40];
        self::assertSame($oldPrize, $actualPrize);
    }

    private function process(UpdatePrizeInput $input, PrizeService $service)
    {
        $useCase = new UpdatePrizeUseCase($service);

        $useCase->process($input);
    }
}
