<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\UseCase\Prize;

use N11t\Tombola\Input\Prize\UpdateBlankInput;
use N11t\Tombola\Service\Prize\BlankService;
use N11t\Tombola\UseCase\Prize\UpdateBlankUseCase;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Input\Prize\FakeUpdateBlankInput;
use Tests\N11t\Tombola\Output\Prize\FakeUpdateBlankOutput;
use Tests\N11t\Tombola\Service\Image\FakeImageService;
use Tests\N11t\Tombola\Service\Prize\FakeBlankService;

class UpdateBlankUseCaseTest extends TestCase
{

    public function testCanUpdateBlank()
    {
        // Arrange
        $expectedBlank = [
            'image' => 'that-was-blank.png',
            'description' => 'That was a blank. Try again'
        ];
        $blankService = new FakeBlankService();
        $input = new FakeUpdateBlankInput([
            'blank' => $expectedBlank
        ]);

        // Act
        $this->process($input, $blankService);

        // Assert
        self::assertSame($expectedBlank, $blankService->blank);
    }

    public function testCanHandleImageNotFound()
    {
        // Arrange
        $expectedBlank = [
            'image' => 'that-was-blank.png',
            'description' => 'That was a blank. Try again'
        ];
        $blankService = new FakeBlankService();
        $input = new FakeUpdateBlankInput([
            'blank' => $expectedBlank
        ]);

        // Act
        $output = $this->process($input, $blankService, $throwImageNotFoundException = true);

        // Assert
        self::assertSame(['ImageNotFound'], $output->errors);
        self::assertSame(FakeBlankService::defaults(), $blankService->blank);
    }

    /**
     * @param UpdateBlankInput $input
     * @param BlankService $blankService
     * @param bool $throwImageNotFoundException
     * @return FakeUpdateBlankOutput
     */
    public function process(
        UpdateBlankInput $input,
        BlankService $blankService,
        bool $throwImageNotFoundException = false
    ): FakeUpdateBlankOutput {
        $imageService = new FakeImageService('/tmp', $throwImageNotFoundException);

        $useCase = new UpdateBlankUseCase($blankService, $imageService);

        $output = new FakeUpdateBlankOutput();

        $useCase->process($input, $output);

        return $output;
    }
}
