<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\UseCase\Image;

use N11t\Tombola\Input\Image\GetImageInput;
use N11t\Tombola\UseCase\Image\GetImageUseCase;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Input\Image\FakeGetImageInput;
use Tests\N11t\Tombola\Output\Image\FakeGetImageOutput;
use Tests\N11t\Tombola\Service\Image\FakeImageService;
use Tests\N11t\Tombola\Service\Prize\FakeBlankService;
use Tests\N11t\Tombola\Service\Prize\FakePrizeService;

class GetImageUseCaseTest extends TestCase
{

    /**
     * @var string
     */
    public $directory;

    public function setUp()
    {
        parent::setUp();

        $this->directory = sys_get_temp_dir();
    }

    public function testCanGetImage()
    {
        // Act
        $output = $this->processPrize([
            'id' => 10,
            'image' => 'buch.jpg'
        ]);

        // Assert
        $expectedFile = rtrim($this->directory, '/') . '/buch.jpg';
        self::assertSame($expectedFile, $output->file);
    }

    public function testCanHandleBlank()
    {
        // Act
        $output = $this->processBlank();

        // Assert
        $blank = FakeBlankService::defaults();
        $expectedFile = rtrim($this->directory, '/') . '/' . $blank['image'];
        self::assertSame($expectedFile, $output->file);
    }

    /**
     * @expectedException \N11t\Tombola\UseCase\Image\GetImageException
     */
    public function testCanHandleImageNotFoundException()
    {
        // Assert
        $prize = [
            'id' => 10,
            'image' => 'buch.jpg'
        ];

        $input = new FakeGetImageInput([
            'id' => 10,
        ]);

        // Act
        $this->process($input, [$prize], true);
    }

    /**
     * Process the {@see GetImageUseCase} with a blank.
     */
    public function processBlank(): FakeGetImageOutput
    {
        // Arrange
        $input = new FakeGetImageInput([
            'id' => 1
        ]);

        // Arrange
        return $this->process($input);
    }

    /**
     * Process the {@see GetImageUseCase} with given prize
     *
     * @param array $prize
     * @return FakeGetImageOutput
     */
    private function processPrize(array $prize): FakeGetImageOutput
    {
        // Arrange
        $input = new FakeGetImageInput([
            'id' => $prize['id']
        ]);

        // Act
        return $this->process($input, [$prize]);
    }

    /**
     * Prozess the {@see GetImageUseCase}.
     *
     * @param GetImageInput $input
     * @param array $prizes
     * @param bool $throwException
     * @return FakeGetImageOutput
     */
    public function process(
        GetImageInput $input,
        array $prizes = [],
        bool $throwException = false
    ): FakeGetImageOutput {
        $prizeService = new FakePrizeService($prizes);
        $blankService = new FakeBlankService();
        $imageService = new FakeImageService($this->directory, $throwException);

        $useCase = new GetImageUseCase($imageService, $prizeService, $blankService);

        $output = new FakeGetImageOutput();

        $useCase->process($input, $output);

        return $output;
    }
}
