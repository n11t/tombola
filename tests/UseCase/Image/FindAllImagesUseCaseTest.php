<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\UseCase\Image;

use N11t\Tombola\UseCase\Image\FindAllImagesUseCase;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Output\Image\FakeFindAllImagesOutput;
use Tests\N11t\Tombola\Service\Image\FakeImageService;

class FindAllImagesUseCaseTest extends TestCase
{

    public function test()
    {
        // Arrange
        $imageService = new FakeImageService();
        $imageService->images = [
            'tisch.png',
            'buch.png'
        ];
        $useCase = new FindAllImagesUseCase($imageService);
        $output = new FakeFindAllImagesOutput();

        // Act
        $useCase->process($output);

        // Assert
        self::assertSame(['tisch.png', 'buch.png'], $output->images);
    }
}
