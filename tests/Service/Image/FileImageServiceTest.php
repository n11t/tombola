<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Service\Image;

use N11t\Tombola\Service\Image\FileImageService;
use Tests\N11t\Tombola\Service\DirectoryBaseServiceTestCase;

class FileImageServiceTest extends DirectoryBaseServiceTestCase
{

    public function testCanFindImage()
    {
        // Arrange
        $this->copyAssetFile('images/tisch.png', 'tisch.png');
        $service = new FileImageService($this->getUnitTestDirectory());

        // Act
        $filePath = $service->findImage([
            'id' => 10,
            'image' => 'tisch.png'
        ]);

        // Asserts
        $expectedFilePath = $this->getUnitTestDirectory(). '/tisch.png';
        self::assertSame($expectedFilePath, $filePath);
    }

    /**
     * @expectedException \N11t\Tombola\Service\Image\ImageNotFoundException
     */
    public function testCanHandleImageNotFound()
    {
        // Arrange
        $service = new FileImageService($this->getUnitTestDirectory());

        // Act
        $service->findImage([
            'id' => 10,
            'image' => 'tisch.png'
        ]);
    }

    public function testCanFindAll()
    {
        // Arrange
        $this->copyAssetFile('/images/tisch.png', 'tisch.png');
        $this->copyAssetFile('/images/buch.png', 'buch.png');

        $service = new FileImageService($this->getUnitTestDirectory());

        // Act
        $images = $service->findAll();

        // Assert
        self::assertSame(['tisch.png', 'buch.png'], $images);
    }
}
