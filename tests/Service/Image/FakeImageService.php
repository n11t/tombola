<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Service\Image;

use N11t\Tombola\Service\Image\ImageNotFoundException;
use N11t\Tombola\Service\Image\ImageService;

class FakeImageService implements ImageService
{

    /**
     * @var string[]
     */
    public $images;

    /**
     * @var string
     */
    private $directory;

    /**
     * @var bool
     */
    private $throwException;

    public function __construct(string $directory = '/tmp', bool $throwException = false)
    {
        $this->directory = rtrim($directory, '/');
        $this->throwException = $throwException;
        $this->images = [];
    }

    /**
     * @param array $prize
     * @return string
     * @throws ImageNotFoundException
     */
    public function findImage(array $prize): string
    {
        $fileName = $this->directory . '/' . $prize['image'];

        if ($this->throwException) {
            throw new ImageNotFoundException($fileName);
        }

        return $fileName;
    }

    /**
     * Find all images and return the basename.
     *
     * @return string[]
     */
    public function findAll(): array
    {
        return $this->images;
    }
}
