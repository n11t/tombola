<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Service\Prize;

use N11t\Tombola\Service\Prize\ImportService;

class FakeImportService implements ImportService
{

    /**
     * @var array
     */
    private $prizes;

    public function __construct(array $prizes = [])
    {
        $this->prizes = [];
        foreach ($prizes as $prize) {
            $this->prizes[$prize['id']] = $prize;
        }
    }

    /**
     * Get all prizes to import
     *
     * @return array
     */
    public function getPrizesToImport(): array
    {
        return $this->prizes;
    }
}
