<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Service\Prize;

use N11t\Tombola\Service\Prize\DuplicatePrizeException;
use N11t\Tombola\Service\Prize\PrizeNotFoundException;
use N11t\Tombola\Service\Prize\PrizeService;

class FakePrizeService implements PrizeService
{

    /**
     * @var array
     */
    public $prizes;

    /**
     * FakePrizeService constructor.
     * @param array $prizes
     */
    public function __construct(array $prizes = [])
    {
        $this->prizes = [];
        foreach ($prizes as $prize) {
            $this->prizes[$prize['id']] = $prize;
        }
    }

    /**
     * @param int $id
     * @return array
     * @throws PrizeNotFoundException
     */
    public function findById(int $id): array
    {
        foreach ($this->prizes as $prize) {
            if ($prize['id'] === $id) {
                return $prize;
            }
        }

        throw new PrizeNotFoundException(['id' => $id]);
    }

    /**
     * {@inheritdoc}
     */
    public function addPrize(array $prize)
    {
        $id = $prize['id'];
        if (array_key_exists($id, $this->prizes)) {
            throw new DuplicatePrizeException($prize);
        }

        $this->prizes[$prize['id']] = $prize;
    }

    /**
     * @param int $id
     */
    public function deletePrize(int $id)
    {
        unset($this->prizes[$id]);
    }

    /**
     * {@inheritdoc}
     */
    public function updatePrize(array $prize)
    {
        $this->prizes[$prize['id']] = $prize;
    }

    /**
     * {@inheritdoc}
     */
    public function findAll(): array
    {
        return $this->prizes;
    }
}
