<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Service\Prize;

use N11t\Tombola\Service\Prize\BlankService;

class FakeBlankService implements BlankService
{

    /**
     * @var array
     */
    public $blank;

    /**
     * FakeBlankService constructor.
     * @param array $blank
     */
    public function __construct(array $blank = [])
    {
        $this->blank = array_merge(
            self::defaults(),
            $blank
        );
    }

    /**
     * Get the default values for a blank
     *
     * @return array
     */
    public static function defaults(): array
    {
        return [
            'iamge' => 'blank.jpg',
            'description' => 'blank'
        ];
    }

    /**
     * @return array
     */
    public function getBlank(): array
    {
        return $this->blank;
    }

    /**
     * Update the blank.
     *
     * @param array $blank
     */
    public function updateBlank(array $blank)
    {
        $this->blank = $blank;
    }
}
