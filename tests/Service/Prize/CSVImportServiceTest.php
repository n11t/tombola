<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Service\Prize;

use N11t\Tombola\Service\Prize\CSVImportService;
use Tests\N11t\Tombola\Service\DirectoryBaseServiceTestCase;

class CSVImportServiceTest extends DirectoryBaseServiceTestCase
{

    public function testCanGetPrizesToImport()
    {
        // Arrange
        $prizes = [
            ['10', '10', 'buch.jpg', 'A book'],
            ['15', '17', 'tisch.jpg', 'A table']
        ];
        $this->putCsvFile($prizes);
        $service = new CSVImportService($this->getCsvFilePath());

        // Act
        $actualPrizes = $service->getPrizesToImport();

        // Assert
        self::assertSame([
            10 => [
                'id' => 10,
                'image' => 'buch.jpg',
                'description' => 'A book',
            ],
            15 => [
                'id' => 15,
                'image' => 'tisch.jpg',
                'description' => 'A table',
            ],
            16 => [
                'id' => 16,
                'image' => 'tisch.jpg',
                'description' => 'A table',
            ],
            17 => [
                'id' => 17,
                'image' => 'tisch.jpg',
                'description' => 'A table',
            ],
        ], $actualPrizes);
    }

    /**
     * @return string
     */
    public function getCsvFilePath(): string
    {
        return $this->getUnitTestDirectory() . '/prizes.csv';
    }

    public function putCsvFile(array $content = [])
    {
        $handle = fopen($this->getCsvFilePath(), 'wb');
        foreach ($content as $row) {
            fputcsv($handle, $row);
        }

        fclose($handle);
    }
}
