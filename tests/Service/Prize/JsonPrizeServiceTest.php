<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Service\Prize;

use N11t\Tombola\Service\Prize\DuplicatePrizeException;
use N11t\Tombola\Service\Prize\JsonPrizeService;
use Tests\N11t\Tombola\Service\DirectoryBaseServiceTestCase;

class JsonPrizeServiceTest extends DirectoryBaseServiceTestCase
{

    public function testCanFindById()
    {
        // Arrange
        $expectedPrice = [
            'id' => 10,
            'image' => 'tisch.jpg',
            'description' => 'A table'
        ];
        $this->writeJsonFile([$expectedPrice]);
        $service = new JsonPrizeService($this->getJsonFilePath());

        // Act
        $actualPrice = $service->findById($expectedPrice['id']);

        // Assert
        self::assertSame($expectedPrice, $actualPrice);
    }

    /**
     * @depends testCanFindById
     */
    public function testCanAddPrize()
    {
        // Arrange
        $expectedPrice = [
            'id' => 10,
            'image' => 'tisch.jpg',
            'description' => 'A table'
        ];
        $this->writeJsonFile();
        $service = new JsonPrizeService($this->getJsonFilePath());

        // Act
        $service->addPrize($expectedPrice);
        $actualPrice = $service->findById($expectedPrice['id']);

        // Assert
        self::assertSame($expectedPrice, $actualPrice);
    }

    /**
     * @expectedException \N11t\Tombola\Service\Prize\PrizeNotFoundException
     * @expectedExceptionMessage Can't find prize for params: {"id":1}
     */
    public function testCanThrowPrizeNotFoundException()
    {
        // Arrange
        $this->writeJsonFile();
        $service = new JsonPrizeService($this->getJsonFilePath());

        // Act
        $service->findById(1);
    }

    public function testCanThrowDuplicateException()
    {
        // Arrange
        $duplicatePrize = [
            'id' => 10,
            'image' => 'tisch.jpg',
            'description' => 'A table'
        ];
        $this->writeJsonFile([$duplicatePrize]);
        $service = new JsonPrizeService($this->getJsonFilePath());

        // Act
        $exception = null;
        try {
            $service->addPrize($duplicatePrize);
        } catch (DuplicatePrizeException $exn) {
            $exception = $exn;
        }

        self::assertInstanceOf(DuplicatePrizeException::class, $exception);
        self::assertSame('Duplicate prize for id 10', $exception->getMessage());
        self::assertCount(1, $this->readJsonFile());
    }

    /**
     * @depends testCanAddPrize
     */
    public function testCanCreateJsonFile()
    {
        // Arrange
        $prize = [
            'id' => 10,
            'image' => 'tisch.jpg',
            'description' => 'A table'
        ];
        $service = new JsonPrizeService($this->getJsonFilePath());

        // Act
        $service->addPrize($prize);

        self::assertCount(1, $this->readJsonFile());
    }

    public function testCanDeletePrize()
    {
        // Arrange
        $prize = [
            'id' => 10,
            'image' => 'tisch.jpg',
            'description' => 'A table'
        ];
        $this->writeJsonFile([$prize]);
        $service = new JsonPrizeService($this->getJsonFilePath());

        // Act
        $service->deletePrize($prize['id']);

        // Assert
        self::assertCount(0, $this->readJsonFile());
    }

    public function testCanUpdatePrize()
    {
        // Arrange
        $prize = [
            'id' => 10,
            'image' => 'tisch.jpg',
            'description' => 'A table'
        ];
        $this->writeJsonFile([$prize]);

        $prize['image'] = 'buch.jpg';
        $prize['description'] = 'A book';

        $service = new JsonPrizeService($this->getJsonFilePath());

        // Act
        $service->updatePrize($prize);

        // Assert
        self::assertCount(1, $this->readJsonFile());
        self::assertSame([
            10 => [
                'id' => 10,
                'image' => 'buch.jpg',
                'description' => 'A book'
            ]
        ], $this->readJsonFile());
    }

    public function testCanFindAll()
    {
        // Arrange
        $expectedPrizes = [
            10 => [
                'id' => 10,
                'image' => 'tisch.jpg',
                'description' => 'a table'
            ],
            30 => [
                'id' => 30,
                'image' => 'tisch.jpg',
                'description' => 'a table'
            ]
        ];

        $this->writeJsonFile($expectedPrizes);
        $service = new JsonPrizeService($this->getJsonFilePath());

        // Act
        $actualPrizes = $service->findAll();

        // Assert
        self::assertCount(2, $this->readJsonFile());
        self::assertSame($expectedPrizes, $actualPrizes);
    }

    /**
     * @return string
     */
    public function getJsonFilePath(): string
    {
        return $this->getUnitTestDirectory() . '/prizes.json';
    }

    public function writeJsonFile(array $prizes = [])
    {
        $data = [];
        foreach ($prizes as $prize) {
            $data[$prize['id']] = $prize;
        }

        $content = json_encode($data);
        file_put_contents($this->getJsonFilePath(), $content);
    }

    public function readJsonFile(): array
    {
        $content = file_get_contents($this->getJsonFilePath());

        return json_decode($content, $assoc = true);
    }
}
