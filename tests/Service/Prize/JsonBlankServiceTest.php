<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Service\Prize;

use N11t\Tombola\Service\Prize\JsonBlankService;
use Tests\N11t\Tombola\Service\DirectoryBaseServiceTestCase;

class JsonBlankServiceTest extends DirectoryBaseServiceTestCase
{

    public function testCanGetBlank()
    {
        // Assert
        $expectedBlank = [
            'image' => 'blank.jpg',
            'description' => 'Blank. Please try again'
        ];
        $this->putBlankFile($expectedBlank);

        $service = new JsonBlankService($this->getBlankFilePath());

        // Arrange
        $blank = $service->getBlank();

        // Assert
        self::assertSame($expectedBlank, $blank);
    }

    public function testCanUpdateBlank()
    {
        // Assert
        $expectedBlank = [
            'image' => 'blank2.jpg',
            'description' => 'Please try again'
        ];
        $this->putBlankFile([
            'image' => 'blank.jpg',
            'description' => 'This was a blank...'
        ]);
        $service = new JsonBlankService($this->getBlankFilePath());

        // Arrange
        $service->updateBlank($expectedBlank);

        // Assert
        $actualFileContent = $this->readJsonFileAsArray($this->getBlankFilePath());
        self::assertSame($expectedBlank, $actualFileContent);
    }

    /**
     * Get the blank file path
     *
     * @return string
     */
    public function getBlankFilePath(): string
    {
        return $this->getUnitTestDirectory() . '/blank.json';
    }

    /**
     * Create blank file with given blank
     *
     * @param array $blank
     */
    public function putBlankFile(array $blank = [])
    {
        $content = json_encode($blank);

        file_put_contents(
            $this->getBlankFilePath(),
            $content
        );
    }
}
