<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Service\Prize;

use N11t\Tombola\Service\Prize\PrizeNotFoundException;
use PHPUnit\Framework\TestCase;

class PrizeNotFoundExceptionTest extends TestCase
{

    public function testCanGetParams()
    {
        // Assert
        $params = [
            'id' => 10,
        ];

        // Act
        $exception = new PrizeNotFoundException($params);

        // Assert
        self::assertSame($params, $exception->getParams());
    }
}
