<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Service;

use PHPUnit\Framework\TestCase;

class DirectoryBaseServiceTestCase extends TestCase
{
    /**
     * @var string
     */
    protected $directory;

    /**
     * @var string
     */
    private $projectDir;

    public function setUp()
    {
        parent::setUp();

        $directory = sys_get_temp_dir() . '/' . $this->getClassName();
        if (\is_dir($directory)) {
            $this->rmdirRecursive($directory);
        }
        \mkdir($directory, $mode = 0755, $recursive = true);

        $this->directory = $directory;
        $this->projectDir = \dirname(__DIR__, 2);
    }

    public function tearDown()
    {
        parent::tearDown();

        $this->rmdirRecursive($this->directory);
    }

    /**
     * @return string
     */
    public function getUnitTestDirectory(): string
    {
        return $this->directory;
    }

    /**
     * Get the tests Assets directory
     *
     * @return string
     */
    protected function getAssetsDirectory(): string
    {
        return $this->projectDir . '/tests/Assets';
    }

    /**
     * Retrun the short class name for current class.
     *
     * @return string
     */
    protected function getClassName(): string
    {
        $reflectionClass = new \ReflectionClass($this);

        return $reflectionClass->getShortName();
    }

    /**
     * Delete given directory recursively.
     *
     * @param string $directory
     */
    protected function rmdirRecursive(string $directory)
    {
        if (\is_dir($directory)) {
            $objects = \scandir($directory, SCANDIR_SORT_ASCENDING);
            foreach ($objects as $object) {
                if ($object !== '.' && $object !== '..') {
                    if (\filetype($directory . "/" . $object) === "dir") {
                        $this->rmdirRecursive($directory . '/' . $object);
                    } else {
                        \unlink($directory . '/' . $object);
                    }
                }
            }
            \reset($objects);
            \rmdir($directory);
        }
    }

    /**
     * Copy the basic file into the unit test directory
     * @param string $assetFile
     * @param string $targetFile
     */
    protected function copyAssetFile(string $assetFile, string $targetFile)
    {
        $source = $this->getAssetsDirectory() . "/{$assetFile}";
        $target = $this->getUnitTestDirectory() . "/{$targetFile}";

        copy($source, $target);
    }

    /**
     * @param string $file
     * @return string
     */
    protected function getFileContent(string $file): string
    {
        return file_get_contents($file);
    }

    /**
     * Read given file as array
     *
     * @param string $file
     * @return array
     */
    protected function readJsonFileAsArray(string $file): array
    {
        $content = $this->getFileContent($file);

        return json_decode($content, $assoc = true);
    }
}
