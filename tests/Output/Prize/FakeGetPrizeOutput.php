<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Output\Prize;

use N11t\Tombola\Output\Prize\GetPrizeOutput;

class FakeGetPrizeOutput implements GetPrizeOutput
{

    /**
     * @var array
     */
    public $prize;

    /**
     * @param array $prize
     */
    public function setPrize(array $prize)
    {
        $this->prize = $prize;
    }
}
