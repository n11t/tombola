<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Output\Prize;

use N11t\Tombola\Output\Prize\SymfonyGetPrizeOutput;
use PHPUnit\Framework\TestCase;

class SymfonyGetPrizeOutputTest extends TestCase
{

    public function test()
    {
        // Arrange
        $output = new SymfonyGetPrizeOutput();

        // Act
        $prize = [
            'id' => 10,
            'image' => 'tisch.jpg',
            'description' => 'A Table'
        ];
        $output->setPrize($prize);

        // Assert
        self::assertSame($prize, $output->getPrize());
    }
}
