<?php

namespace Tests\N11t\Tombola\Output\Prize;

use N11t\Tombola\Output\Prize\FindAllPrizesOutput;

class FakeFindAllPrizesOutput implements FindAllPrizesOutput
{

    /**
     * @var array
     */
    public $prizes;

    /**
     * Set the prizes.
     *
     * @param array $prizes
     */
    public function setPrizes(array $prizes)
    {
        $this->prizes = $prizes;
    }
}
