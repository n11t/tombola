<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Output\Prize;

use N11t\Tombola\Output\Prize\UpdateBlankOutput;

class FakeUpdateBlankOutput implements UpdateBlankOutput
{

    /**
     * @var array
     */
    public $errors;

    public function __construct()
    {
        $this->errors = [];
    }

    /**
     * Add an error thrown by validating the blank.
     *
     * @param array $blank
     * @param string $error
     */
    public function addBlankError(array $blank, string $error)
    {
        $this->errors[] = $error;
    }
}
