<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Output\Prize;

use N11t\Tombola\Output\Prize\ImportPrizesOutput;

class FakeImportPrizesOutput implements ImportPrizesOutput
{

    /**
     * @var array
     */
    public $errors;

    /**
     * FakeImportPrizeOutput constructor.
     */
    public function __construct()
    {
        $this->errors = [];
    }

    /**
     * Add an error for given prize.
     *
     * @param array $prize
     * @param string $error
     */
    public function addError(array $prize, string $error)
    {
        $this->errors[$prize['id']] = $error;
    }
}
