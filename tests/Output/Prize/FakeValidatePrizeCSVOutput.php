<?php

namespace Tests\N11t\Tombola\Output\Prize;

use N11t\Tombola\Output\Prize\ValidatePrizeCSVOutput;

class FakeValidatePrizeCSVOutput implements ValidatePrizeCSVOutput
{

    /**
     * @var array
     */
    public $errors;

    /**
     * FakeValidatePrizeCSVOutput constructor.
     */
    public function __construct()
    {
        $this->errors = [];
    }

    public function addFileError(string $error)
    {
        $this->errors['file'][] = $error;
    }

    public function setRowErrors(array $errors)
    {
        $this->errors['rows'] = $errors;
    }
}
