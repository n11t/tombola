<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Output\Prize;

use N11t\Tombola\Output\Prize\GetBlankOutput;

class FakeGetBlankOutput implements GetBlankOutput
{

    /**
     * @var array
     */
    public $blank;

    /**
     * Set the blank
     *
     * @param array $blank
     */
    public function setBlank(array $blank)
    {
        $this->blank = $blank;
    }
}
