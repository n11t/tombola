<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Output\Prize;

use N11t\Tombola\Output\Prize\ArrayGetBlankOutput;
use PHPUnit\Framework\TestCase;

class ArrayGetBlankOutputTest extends TestCase
{

    public function test()
    {
        // Arrange
        $blank = [
            'image' => 'blank.png',
            'description' => 'A blank.'
        ];
        $output = new ArrayGetBlankOutput();

        // Act
        $output->setBlank($blank);

        // Assert
        self::assertSame($blank, $output->toArray());
    }

    public function testCanHandleDefault()
    {
        // Act
        $output = new ArrayGetBlankOutput();

        // Assert
        self::assertSame([], $output->toArray());
    }
}
