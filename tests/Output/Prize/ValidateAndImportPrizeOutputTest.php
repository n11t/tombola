<?php

namespace Tests\N11t\Tombola\Output\Prize;

use N11t\Tombola\Output\Prize\ValidateAndImportPrizeOutput;
use PHPUnit\Framework\TestCase;

class ValidateAndImportPrizeOutputTest extends TestCase
{

    public function test()
    {
        // Arrange
        $fileErrors = [
            'NotFound',
            'NotReadable'
        ];
        $rowErrors = [
            1 => 'ImageNotFound',
            2 => 'DuplicateNumber'
        ];
        $prizeErrors = [
            10 => 'Duplicate',
            12 => 'Duplicate'
        ];
        $output = new ValidateAndImportPrizeOutput();

        // Act
        foreach ($fileErrors as $error) {
            $output->addFileError($error);
        }

        foreach ($prizeErrors as $id => $error) {
            $output->addError(['id' => $id], $error);
        }

        $output->setRowErrors($rowErrors);

        // Assert
        self::assertEquals([
            'file' => $fileErrors,
            'rows' => $rowErrors,
            'prize' => $prizeErrors
        ], $output->toArray());
        self::assertSame($fileErrors, $output->getFileErrors());
        self::assertSame($rowErrors, $output->getRowErrors());
        self::assertSame($prizeErrors, $output->getPrizeErrors());
    }

    public function testCanHandleNoErrors()
    {
        // Arrange
        $output = new ValidateAndImportPrizeOutput();

        // Assert
        self::assertEquals([
            'file' => [],
            'rows' => [],
            'prize' => []
        ], $output->toArray());
        self::assertSame([], $output->getFileErrors());
        self::assertSame([], $output->getRowErrors());
        self::assertSame([], $output->getPrizeErrors());
    }
}
