<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Output\Prize;

use N11t\Tombola\Output\Prize\ArrayUpdateBlankOutput;
use PHPUnit\Framework\TestCase;
use Tests\N11t\Tombola\Service\Prize\FakeBlankService;

class ArrayUpdateBlankOutputTest extends TestCase
{

    public function test()
    {
        // Arrange
        $outputWithErrors = new ArrayUpdateBlankOutput();
        $outputWithoutErrors = new ArrayUpdateBlankOutput();

        // Act
        $outputWithErrors->addBlankError(FakeBlankService::defaults(), 'ImageNotFound');

        // Assert
        self::assertFalse($outputWithErrors->isSuccess());
        self::assertTrue($outputWithoutErrors->isSuccess());

        self::assertSame(['ImageNotFound'], $outputWithErrors->toArray());
        self::assertSame([], $outputWithoutErrors->toArray());
    }
}
