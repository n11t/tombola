<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Output\Prize;

use N11t\Tombola\Output\Prize\ArrayCreatePrizeOutput;
use PHPUnit\Framework\TestCase;

class ArrayCreatePrizeOutputTest extends TestCase
{

    public function test()
    {
        // Arrange
        $output = new ArrayCreatePrizeOutput();

        // Act
        $output->setCreatePrizeError(['id'=>20], 'Duplicate');

        // Assert
        self::assertFalse($output->isSuccess());
        self::assertSame([
            'errors' => ['Duplicate']
        ], $output->toArray());
    }
}
