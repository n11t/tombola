<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Output\Prize;

use N11t\Tombola\Output\Prize\CreatePrizeOutput;

class FakeCreatePrizeOutput implements CreatePrizeOutput
{

    /**
     * @var array
     */
    public $errors;

    public function __construct()
    {
        $this->errors = [];
    }

    /**
     * @param array $prize
     * @param string $error
     */
    public function setCreatePrizeError(array $prize, string $error)
    {
        $this->errors[] = $error;
    }
}
