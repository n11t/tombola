<?php

namespace Tests\N11t\Tombola\Output\Prize;

use N11t\Tombola\Output\Prize\ArrayFindAllPrizesOutput;
use PHPUnit\Framework\TestCase;

class ArrayFindAllPrizesOutputTest extends TestCase
{

    public function test()
    {
        // Arrange
        $expectedPrizes = [
            [
                'id' => 10,
                'image' => 'buch.jpg',
                'description' => 'A book'
            ],
            [
                'id' => 30,
                'image' => 'tisch.png',
                'description' => 'A table'
            ]
        ];
        $output = new ArrayFindAllPrizesOutput();

        // Act
        $output->setPrizes($expectedPrizes);

        // Assert
        self::assertSame($expectedPrizes, $output->toArray());
    }
}
