<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Output\Image;

use N11t\Tombola\Output\Image\SymfonyGetImageOutput;
use PHPUnit\Framework\TestCase;

class SymfonyGetImageOutputTest extends TestCase
{

    public function test()
    {
        // Arrange
        $output = new SymfonyGetImageOutput();

        // Act
        $output->setFilename('asdf');

        // Assert
        self::assertSame('asdf', $output->getFileName());
    }
}
