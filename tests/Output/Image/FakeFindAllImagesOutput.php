<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Output\Image;

use N11t\Tombola\Output\Image\FindAllImagesOutput;

class FakeFindAllImagesOutput implements FindAllImagesOutput
{

    /**
     * @var string[]
     */
    public $images;

    /**
     * Set the basenames as string array.
     *
     * @param array $images
     */
    public function setImages(array $images)
    {
        $this->images = $images;
    }
}
