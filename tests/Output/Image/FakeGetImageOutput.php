<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Output\Image;

use N11t\Tombola\Output\Image\GetImageOutput;

class FakeGetImageOutput implements GetImageOutput
{

    /**
     * @var string
     */
    public $file;

    /**
     * @param string $fileName
     */
    public function setFilename(string $fileName)
    {
        $this->file = $fileName;
    }
}
