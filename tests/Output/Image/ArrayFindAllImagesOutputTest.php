<?php
declare(strict_types=1);

namespace Tests\N11t\Tombola\Output\Image;

use N11t\Tombola\Output\Image\ArrayFindAllImagesOutput;
use PHPUnit\Framework\TestCase;

class ArrayFindAllImagesOutputTest extends TestCase
{

    public function test()
    {
        // Arrange
        $output = new ArrayFindAllImagesOutput();

        // Act
        $output->setImages(['tisch.png', 'buch.png']);

        // Assert
        self::assertSame(['tisch.png', 'buch.png'], $output->toArray());
    }
}
