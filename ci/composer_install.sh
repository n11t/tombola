#!/bin/bash
if [ ! -f composer.phar ]; then
    wget https://composer.github.io/installer.sig -O - -q | tr -d '\n' > installer.sig
    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
    php composer-setup.php
    php -r "unlink('composer-setup.php'); unlink('installer.sig');"
else
    php composer.phar self-update --no-progress
fi

if [ -f composer.lock ]; then
    php composer.phar update --no-progress --optimize-autoloader
else
    php composer.phar install --no-progress --optimize-autoloader
fi
