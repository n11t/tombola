#!/usr/bin/env bash

find src tests -type f -name '*.php' -exec php -l {} \; |grep -v "No syntax errors detected"

RC=$?
if [[ "x$RC" == "x0" ]]; then
    exit 1
fi

exit 0
